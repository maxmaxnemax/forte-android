package com.max.forte.asyncTasks;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public abstract class PostName extends AsyncTask<String, Void, Boolean> {

    private static final String TAG = "PostName";
    private static final String TAG_SUCCESS = "success";
    private static final String POST_NAME_URL = "http://vast-reef-9282.herokuapp.com/post_name.php";

    protected Uri.Builder mUriBuilder;

    public PostName() {
        mUriBuilder = Uri.parse(POST_NAME_URL).buildUpon();
    }

    @Override
    protected Boolean doInBackground(String... params) {
        Log.d(TAG, "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
        Log.d(TAG, "params[0] " + params[0]);
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormEncodingBuilder()
                .add("name", params[0])
                .add("other", null)
                .build();
        Request request = new Request.Builder()
                .url(POST_NAME_URL)
                .post(formBody)
                .build();
        try {
            Response response = client.newCall(request).execute();
            JSONObject jObj = new JSONObject(response.body().string());
            Log.d("DELETE CLOTHES: ", response.body().string());
            if (jObj.getInt(TAG_SUCCESS) == 1) // Checking for SUCCESS TAG
                return true;
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return false;
    }
}
