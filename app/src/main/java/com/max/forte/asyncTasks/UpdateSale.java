package com.max.forte.asyncTasks;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;

import com.max.forte.model.Clothes;
import com.max.forte.model.Sale;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public abstract class UpdateSale extends AsyncTask<Pair<Sale, Clothes>, Void, Boolean> {

    private static final String TAG = "UpdateSale";
    private static final String TAG_SUCCESS = "success";
    private static final String UPDATE_SALE_URL = "http://vast-reef-9282.herokuapp.com/update_sale.php";

    protected Uri.Builder mUriBuilder;

    public UpdateSale() {
        mUriBuilder = Uri.parse(UPDATE_SALE_URL).buildUpon();
    }

    @SafeVarargs
    @Override
    protected final Boolean doInBackground(Pair<Sale, Clothes>... pairs) {
        Log.d(TAG, "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
        Sale sale = pairs[0].first;
        Clothes clothes = pairs[0].second;
        Log.d(TAG, "sale " + sale);
        Log.d(TAG, "clothes " + clothes);

        OkHttpClient client = new OkHttpClient();
        FormEncodingBuilder builder = new FormEncodingBuilder();
        builder.add("clothesId", sale.getClothesIdString())
                .add("name", sale.getName())
                .add("model", sale.getModel());
        if (sale.getSizesList() != null)
            for (String size : sale.getSizesList()) {
                builder.add("sizes[]", size);
            }
        builder.add("quantity", String.valueOf(sale.getQuantity()))
                .add("buyPrice", sale.getBuyPriceString())
                .add("salePrice", sale.getSalePriceString())
                .add("entranceDate", sale.getEntranceDateStringFormatted())
                .add("saleDate", sale.getSaleDateStringFormatted())
                .add("marks", sale.getMarks())
                .add("id", sale.getIdString());

        if (clothes.getSizesNotSold() != null)
            for (String size : clothes.getSizesNotSold()) {
                builder.add("sizesNotSold[]", size);
            }
        if (clothes.getSizesSold() != null)
            for (String size : clothes.getSizesSold()) {
                builder.add("sizesSold[]", size);
            }
        builder.add("quantityNotSold", String.valueOf(clothes.getQuantityNotSold()));
        RequestBody formBody = builder.build();
        Request request = new Request.Builder()
                .url(UPDATE_SALE_URL)
                .post(formBody)
                .build();
        try {
            Response response = client.newCall(request).execute();
            JSONObject jObj = new JSONObject(response.body().string());
            Log.d("UPDATE SALE", response.body().string());
            if (jObj.getInt(TAG_SUCCESS) == 1) // Checking for SUCCESS TAG
                return true;
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return false;
    }
}
