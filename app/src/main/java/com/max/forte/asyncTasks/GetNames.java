package com.max.forte.asyncTasks;

import android.net.Uri;
import android.os.AsyncTask;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public abstract class GetNames extends AsyncTask<String, Void, ArrayList<String>> {

    private static final String TAG = "GetNames";
    private static final String TAG_NAMES = "names";
    private static final String TAG_SUCCESS = "success";
    private static final String NAMES_URL = "http://vast-reef-9282.herokuapp.com/get_names.php";

    protected Uri.Builder mUriBuilder;

    public GetNames() {
        mUriBuilder = Uri.parse(NAMES_URL).buildUpon();
    }

    @Override
    protected ArrayList<String> doInBackground(String... params) {
        ArrayList<String> namesList = new ArrayList<>();

        OkHttpClient httpClient = new OkHttpClient();
        Request request = new Request.Builder().url(NAMES_URL).build();
        try {
            Response response = httpClient.newCall(request).execute();
            JSONObject json = new JSONObject(response.body().string());
            JSONArray namesArray = json.getJSONArray(TAG_NAMES);
            for (int i = 0; i < namesArray.length(); i++) {
                JSONObject c = namesArray.getJSONObject(i);

                // Storing each json item in variable
                String name = c.getString("name");
                namesList.add(name);
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return namesList;
    }

}
