package com.max.forte.asyncTasks;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;

import com.max.forte.model.Clothes;
import com.max.forte.model.Sale;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public abstract class DeleteSale extends AsyncTask<Pair<Sale, Clothes>, Void, Boolean> {

    private static final String TAG = "DeleteSale";
    private static final String TAG_SUCCESS = "success";
    private static final String DELETE_SALE_URL = "http://vast-reef-9282.herokuapp.com/delete_sale.php";

    protected Uri.Builder mUriBuilder;

    public DeleteSale() {
        mUriBuilder = Uri.parse(DELETE_SALE_URL).buildUpon();
    }

    @SafeVarargs
    @Override
    protected final Boolean doInBackground(Pair<Sale, Clothes>... pairs) {
        Log.d(TAG, "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
        Sale saleToDelete = pairs[0].first;
        Clothes clothes = pairs[0].second;
        Log.d(TAG, "saleToDelete " + saleToDelete);
        Log.d(TAG, "clothes " + clothes);
        OkHttpClient client = new OkHttpClient();
        FormEncodingBuilder builder = new FormEncodingBuilder();
        builder.add("saleDate", saleToDelete.getSaleDateStringFormatted());
        builder.add("id", saleToDelete.getIdString());

        if (clothes.getSizesNotSold() != null)
            for (String size : clothes.getSizesNotSold()) {
                builder.add("sizesNotSold[]", size);
            }
        if (clothes.getSizesSold() != null)
            for (String size : clothes.getSizesSold()) {
                builder.add("sizesSold[]", size);
            }
        builder.add("quantityNotSold", String.valueOf(clothes.getQuantityNotSold()));
        builder.add("clothesId", saleToDelete.getClothesIdString());


        RequestBody formBody = builder.build();
        Request request = new Request.Builder()
                .url(DELETE_SALE_URL)
                .post(formBody)
                .build();
        try {
            Response response = client.newCall(request).execute();
            JSONObject jObj = new JSONObject(response.body().string());
            Log.d("DELETE CLOTHES: ", response.body().string());
            if (jObj.getInt(TAG_SUCCESS) == 1) // Checking for SUCCESS TAG
                return true;
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return false;
    }
}
