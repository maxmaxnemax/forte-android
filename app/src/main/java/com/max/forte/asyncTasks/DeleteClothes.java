package com.max.forte.asyncTasks;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public abstract class DeleteClothes extends AsyncTask<Integer, Void, Boolean> {

    private static final String TAG = "DeleteClothes";
    private static final String TAG_SUCCESS = "success";
    private static final String DELETE_CLOTHES_URL = "http://vast-reef-9282.herokuapp.com/delete_clothes.php";

    protected Uri.Builder mUriBuilder;

    public DeleteClothes() {
        mUriBuilder = Uri.parse(DELETE_CLOTHES_URL).buildUpon();
    }

    @Override
    protected Boolean doInBackground(Integer... idArray) {
        Log.d(TAG, "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
        int id = idArray[0];
        OkHttpClient client = new OkHttpClient();
        RequestBody formBody = new FormEncodingBuilder()
                .add("id", String.valueOf(id))
                .build();
        Request request = new Request.Builder()
                .url(DELETE_CLOTHES_URL)
                .post(formBody)
                .build();
        try {
            Response response = client.newCall(request).execute();
            JSONObject jObj = new JSONObject(response.body().string());
            Log.d("DELETE CLOTHES: ", response.body().string());
            if (jObj.getInt(TAG_SUCCESS) == 1) // Checking for SUCCESS TAG
                return true;
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return false;
    }
}
