package com.max.forte.asyncTasks;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.max.forte.model.CalendarDay;
import com.max.forte.model.Sale;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public abstract class GetSales extends AsyncTask<String, Void, ArrayList<Sale>> {

    private static final String TAG = "GetSales";
    private static final String SALES_URL = "http://vast-reef-9282.herokuapp.com/get_sales.php";

    protected Uri.Builder mUriBuilder;
    private static final boolean debug = false;
    private final String saleDateFilter;

    public GetSales(String saleDateFilter) {
        mUriBuilder = Uri.parse(SALES_URL).buildUpon();
        this.saleDateFilter = saleDateFilter;
    }

    @Override
    protected ArrayList<Sale> doInBackground(String... params) {
        Log.d(TAG, "GetSales -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
        Log.d(TAG, "saleDateFilter " + saleDateFilter);

        ArrayList<Sale> salesList = new ArrayList<>();
        OkHttpClient httpClient = new OkHttpClient();
        Request request = new Request.Builder().url(SALES_URL).build();

        try {
            Response response = httpClient.newCall(request).execute();
            JSONObject json = new JSONObject(response.body().string());
            JSONArray salesArray = json.getJSONArray("sales");
            for (int i = 0; i < salesArray.length(); i++) {
                ArrayList<String> entranceDateArray, saleDateArray;
                CalendarDay entranceDate, saleDate;
                JSONObject salesItem = salesArray.getJSONObject(i);

                String entranceDateString = salesItem.getString("entrancedate");
                if (entranceDateString != null) {
                    entranceDateArray = new ArrayList<>(Arrays.asList(entranceDateString.replaceAll("[{}]", "").split("-")));
                    if (debug) {
                        for (String en : entranceDateArray)
                            Log.d(TAG, "en " + en);
                    }
                    entranceDate = new CalendarDay(Integer.parseInt(entranceDateArray.get(0)), Integer.parseInt(entranceDateArray.get(1)), Integer.parseInt(entranceDateArray.get(2)));
                } else
                    entranceDate = new CalendarDay();

                String saleDateString = salesItem.getString("saledate");
                if (saleDateString != null) {
                    saleDateArray = new ArrayList<>(Arrays.asList(saleDateString.replaceAll("[{}]", "").split("-")));
                    if (debug) {
                        for (String en : saleDateArray)
                            Log.d(TAG, "en " + en);
                    }
                    saleDate = new CalendarDay(Integer.parseInt(saleDateArray.get(0)), Integer.parseInt(saleDateArray.get(1)), Integer.parseInt(saleDateArray.get(2)));
                } else
                    saleDate = new CalendarDay();

                Sale s = new Sale(salesItem.getInt("id"), salesItem.getInt("clothesid"), salesItem.getString("name"), salesItem.getString("model"),
                        salesItem.getString("sizes").replaceAll("[{}]", ""), salesItem.getInt("quantity"), Integer.parseInt(salesItem.getString("buyprice")),
                        Integer.parseInt(salesItem.getString("saleprice")), entranceDate, saleDate, salesItem.getString("marks"));
                salesList.add(s);
            }
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return salesList;
    }

}
