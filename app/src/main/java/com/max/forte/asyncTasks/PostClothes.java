package com.max.forte.asyncTasks;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.max.forte.model.Clothes;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public abstract class PostClothes extends AsyncTask<Clothes, Void, Integer> {

    private static final String TAG = "PostClothes";
    private static final String TAG_SUCCESS = "success";
    private static final String POST_CLOTHES_URL = "http://vast-reef-9282.herokuapp.com/post_clothes.php";

    protected Uri.Builder mUriBuilder;

    public PostClothes() {
        mUriBuilder = Uri.parse(POST_CLOTHES_URL).buildUpon();
    }

    @Override
    protected Integer doInBackground(Clothes... clothesList) {
        Log.d(TAG, "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
        Clothes clothes = clothesList[0];

        OkHttpClient client = new OkHttpClient();
        FormEncodingBuilder builder = new FormEncodingBuilder();

        builder.add("name", clothes.getName())
                .add("model", clothes.getModel());
        if (clothes.getSizesNotSold() != null)
            for (String size : clothes.getSizesNotSold()) {
                builder.add("sizesNotSold[]", size);
            }
        if (clothes.getSizesSold() != null)
            for (String size : clothes.getSizesSold()) {
                builder.add("sizesSold[]", size);
            }
        builder.add("allQuantity", String.valueOf(clothes.getAllQuantity()))
                .add("quantityNotSold", String.valueOf(clothes.getQuantityNotSold()))
                .add("buyPrice", clothes.getBuyPriceString())
                .add("salePrice", clothes.getSalePriceString())
                .add("entranceDate", clothes.getEntranceDateStringFormatted())
                .add("marks", clothes.getMarks());

        RequestBody formBody = builder.build();
        Request request = new Request.Builder()
                .url(POST_CLOTHES_URL)
                .post(formBody)
                .build();
        try {
            Response response = client.newCall(request).execute();
            JSONObject jObj = new JSONObject(response.body().string());
            Log.d("POST CLOTHES", response.body().string());
            if (jObj.getInt(TAG_SUCCESS) == 1) // Checking for SUCCESS TAG
                return jObj.getInt("id");
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return -1;
    }
}
