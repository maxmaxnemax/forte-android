package com.max.forte.asyncTasks;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;
import android.util.Pair;

import com.max.forte.model.Clothes;
import com.max.forte.model.Sale;
import com.squareup.okhttp.FormEncodingBuilder;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

public abstract class PostSale extends AsyncTask<Pair<Sale, Clothes>, Void, Integer> {

    private static final String TAG = "PostSale";
    private static final String TAG_SUCCESS = "success";
    private static final String POST_SALE_URL = "http://vast-reef-9282.herokuapp.com/post_sale.php";

    protected Uri.Builder mUriBuilder;

    public PostSale() {
        mUriBuilder = Uri.parse(POST_SALE_URL).buildUpon();
    }

    @SafeVarargs
    @Override
    protected final Integer doInBackground(Pair<Sale, Clothes>... pairs) {
        Log.d(TAG, "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
        Sale sale = pairs[0].first;
        Clothes clothes = pairs[0].second;
        Log.d(TAG, "sale " + sale);
        Log.d(TAG, "clothes " + clothes);

        OkHttpClient client = new OkHttpClient();
        FormEncodingBuilder builder = new FormEncodingBuilder();
        builder.add("clothesId", sale.getClothesIdString())
                .add("name", sale.getName())
                .add("model", sale.getModel());
        if (sale.getSizesList() != null)
            for (String size : sale.getSizesList()) {
                builder.add("sizes[]", size);
            }
        builder.add("quantity", String.valueOf(sale.getQuantity()))
                .add("salePrice", sale.getSalePriceString())
                .add("buyPrice", sale.getBuyPriceString())
                .add("entranceDate", sale.getEntranceDateStringFormatted())
                .add("saleDate", sale.getSaleDateStringFormatted())
                .add("marks", sale.getMarks());

        if (clothes.getSizesNotSold() != null)
            for (String size : clothes.getSizesNotSold()) {
                builder.add("sizesNotSold[]", size);
                Log.d(TAG, "add sizeNotSold " + size);
            }
        if (clothes.getSizesSold() != null)
            for (String size : clothes.getSizesSold()) {
                builder.add("sizesSold[]", size);
                Log.d(TAG, "add sizeSold " + size);
            }
        builder.add("quantityNotSold", String.valueOf(clothes.getQuantityNotSold()));

        Log.d(TAG, "clothes.getQuantityNotSold() " + clothes.getQuantityNotSold());
        RequestBody formBody = builder.build();
        Request request = new Request.Builder()
                .url(POST_SALE_URL)
                .post(formBody)
                .build();
        try {
            Response response = client.newCall(request).execute();
            JSONObject jObj = new JSONObject(response.body().string());
            Log.d("POST SALE", response.body().string());
            if (jObj.getInt(TAG_SUCCESS) == 1) // Checking for SUCCESS TAG
                return jObj.getInt("id");
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }
        return -1;
    }
}
