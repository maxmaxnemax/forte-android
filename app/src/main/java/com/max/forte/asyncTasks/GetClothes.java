package com.max.forte.asyncTasks;

import android.net.Uri;
import android.os.AsyncTask;
import android.util.Log;

import com.max.forte.model.CalendarDay;
import com.max.forte.model.Clothes;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public abstract class GetClothes extends AsyncTask<String, Void, ArrayList<Clothes>> {

    private static final String TAG = "GetClothes";
    private static final String CLOTHES_URL = "http://vast-reef-9282.herokuapp.com/get_clothes.php";

    protected Uri.Builder mUriBuilder;
    private static final boolean debug = false;

    public GetClothes() {
        mUriBuilder = Uri.parse(CLOTHES_URL).buildUpon();
    }

    @Override
    protected ArrayList<Clothes> doInBackground(String... params) {
        Log.d(TAG, "GetClothes -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");

        ArrayList<Clothes> clothesList = new ArrayList<>();
        OkHttpClient httpClient = new OkHttpClient();
        Request request = new Request.Builder().url(CLOTHES_URL).build();
        try {
            Response response = httpClient.newCall(request).execute();
            JSONObject json = new JSONObject(response.body().string());
            JSONArray clothesArray = json.getJSONArray("clothes");
            for (int i = 0; i < clothesArray.length(); i++) {
                ArrayList<String> sizesNotSold, sizesSold, entranceDateArray;
                CalendarDay entranceDate;
                JSONObject clothesItem = clothesArray.getJSONObject(i);

                String sizesNotSoldString = clothesItem.getString("sizesnotsold");
                if (sizesNotSoldString != null) {
                    Log.d(TAG, "sizesNotSoldString.length " + sizesNotSoldString.length());
                    sizesNotSold = new ArrayList<>(Arrays.asList(sizesNotSoldString.replaceAll("[{}]", "").split(",")));
                    if (sizesNotSold.size() > 0 && sizesNotSold.get(sizesNotSold.size() - 1).equals(","))
                        sizesNotSold.remove(sizesNotSold.size() - 1);
                } else
                    sizesNotSold = new ArrayList<>();

                String sizesSoldString = clothesItem.getString("sizessold");
                if (sizesSoldString != null) {
                    Log.d(TAG, "sizesSoldString.length " + sizesSoldString.length());
                    sizesSold = new ArrayList<>(Arrays.asList(sizesSoldString.replaceAll("[{}]", "").split(",")));
                    if (sizesSold.size() > 0 && sizesSold.get(sizesSold.size() - 1).equals(","))
                        sizesSold.remove(sizesSold.size() - 1);
                } else
                    sizesSold = new ArrayList<>();

                String entranceDateString = clothesItem.getString("entrancedate");
                if (entranceDateString != null) {
                    entranceDateArray = new ArrayList<>(Arrays.asList(entranceDateString.replaceAll("[{}]", "").split("-")));
                    if (debug) {
                        for (String en : entranceDateArray)
                            Log.d(TAG, "en " + en);
                    }
                    if (entranceDateArray.size() > 2)
                        entranceDate = new CalendarDay(Integer.parseInt(entranceDateArray.get(0)), Integer.parseInt(entranceDateArray.get(1)), Integer.parseInt(entranceDateArray.get(2)));
                    else
                        entranceDate = new CalendarDay();
                } else
                    entranceDate = new CalendarDay();

                Clothes c = new Clothes(clothesItem.getInt("id"), clothesItem.getString("name"), clothesItem.getString("model"),
                        sizesNotSold, sizesSold, clothesItem.getInt("allquantity"), clothesItem.getInt("quantitynotsold"), Integer.parseInt(clothesItem.getString("buyprice")),
                        Integer.parseInt(clothesItem.getString("saleprice")), entranceDate, clothesItem.getString("marks"));
                //Log.d(TAG, "c " + c.toString());
                clothesList.add(c);
            }
        } catch (JSONException | IOException e) {
            e.printStackTrace();
        }
        return clothesList;
    }

}
