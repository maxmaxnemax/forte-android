package com.max.forte.adapters;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import com.max.forte.fragments.ClothesFragment;
import com.max.forte.fragments.SalesFragment;

public class PagerAdapter extends FragmentPagerAdapter {

    private ClothesFragment clothesFragment;
    private SalesFragment salesFragment;
    Context ctx;
    private static final String TAG = "PagerAdapter";
    private static final boolean debug = false;

    public PagerAdapter(FragmentManager fm, Context ctx) {
        super(fm);
        this.ctx = ctx;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Fragment getItem(int position) {
        if (position == 0) {
            if (clothesFragment != null)
                return clothesFragment;
            else {
                clothesFragment = new ClothesFragment();
                return clothesFragment;
            }
        } else {
            if (salesFragment != null)
                return salesFragment;
            else
                salesFragment = new SalesFragment();
            return salesFragment;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Log.d(TAG, "getPageTitle " + position);
        if (position == 0)
            return "�����";
        else
            return "�������";
    }

    @Override
    public int getItemPosition(Object obj) {
        return POSITION_UNCHANGED;
    }

    public void showClothes(String chosenName) {    //  called on drawer item click (if pos == 0)
        Log.d(TAG, "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
        Log.d(TAG, "showClothes " + chosenName);
        Bundle nameBundle = new Bundle();
        if (chosenName != null)
            nameBundle.putString("name", chosenName);
        if (clothesFragment == null) {
            clothesFragment = ClothesFragment.newInstance(chosenName);
            notifyDataSetChanged();
        } else {
            clothesFragment.filterClothesList(chosenName);
            clothesFragment.notifyAdapter();
        }
    }

    public void showSales(String chosenName) {  //  called on drawer item click (if pos == 1)
        if (debug) {
            Log.d(TAG, "-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=");
            Log.d(TAG, "showSales " + chosenName);
        }
        Bundle nameBundle = new Bundle();
        if (chosenName != null)
            nameBundle.putString("name", chosenName);
        if (salesFragment == null) {
            salesFragment = SalesFragment.newInstance(chosenName);
            notifyDataSetChanged();
        } else {
            salesFragment.filterSalesList(chosenName);
            salesFragment.notifyAdapter();
        }
    }

    public void notifyClothesChanges() {
        clothesFragment.notifyChanges();
    }

    public void notifySalesChanges() {
        salesFragment.notifyChanges();
    }

}