package com.max.forte.adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class SaleSizesAdapter extends ArrayAdapter<String> {

    boolean[] sizesClicked;
    List<String> sizes;
    Context ctx;
    private static final String TAG = "SaleSizesAdapter";

    public SaleSizesAdapter(Context context, int resource, List<String> sizes) {
        super(context, resource, sizes);
        //Log.d(TAG, "sizes.size() " + sizes.size());
        ctx = context;
        this.sizes = sizes;
        sizesClicked = new boolean[sizes.size()];
        //Log.d(TAG, "sizesClicked.length " + String.valueOf(sizesClicked.length));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        //Log.d(TAG, "getView " + position + ", " + sizesClicked[position]);

        TextView label = (TextView) convertView;
        if (convertView == null) {
            convertView = new TextView(ctx);
            label = (TextView) convertView;
            label.setTextSize(20.0f);
        }
        label.setText(sizes.get(position));

        return convertView;
    }

    @Override
    public boolean isEnabled(int position) {
        return !sizesClicked[position];
    }

    public void addClick(int position) {
        sizesClicked[position] = true;
    }

}
