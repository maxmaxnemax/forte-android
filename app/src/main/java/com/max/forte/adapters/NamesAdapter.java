package com.max.forte.adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.max.forte.R;
import com.max.forte.Utils;
import com.max.forte.asyncTasks.DeleteName;
import com.max.forte.asyncTasks.UpdateName;
import com.max.forte.data_ops.NamesOps;

import java.util.List;

public class NamesAdapter extends RecyclerView.Adapter<NamesAdapter.ViewHolder> {

    private List<String> namesList;
    Context ctx;

    private static final String TAG = "NamesAdapter";

    public NamesAdapter(List<String> records, Context ctx) {
        this.namesList = records;
        this.ctx = ctx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.name_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int i) {
        String name = namesList.get(i);
        viewHolder.name.setText(name);
        viewHolder.deleteButtonListener.setName(name);
    }

    @Override
    public int getItemCount() {
        return namesList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView name;
        private Button deleteButton;
        private DeleteButtonListener deleteButtonListener;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name_name);
            name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "shit");/////   change name's name, save names
                    final Dialog dialog = new Dialog(ctx);
                    dialog.setContentView(R.layout.change_name_dialog);
                    dialog.setTitle(ctx.getString(R.string.modify_name));
                    final EditText nameEditText = (EditText) dialog.findViewById(R.id.nameName);
                    nameEditText.setText(name.getText().toString());

                    Button save = (Button) dialog.findViewById(R.id.save);
                    save.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            final String newName = nameEditText.getText().toString().replaceAll("[\\\\p{Alphabetic} ]*", "");
                            Log.d(TAG, "name: " + name.getText().toString());
                            if(Utils.isNetworkConnected(ctx)) {
                                new UpdateName() {
                                    @Override
                                    public void onPostExecute(Boolean updated) {
                                        if (updated) {
                                            namesList.remove(name.getText().toString());
                                            namesList.add(newName);
                                            NamesOps.saveNames(ctx, namesList);

                                            Toast.makeText(ctx, ctx.getString(R.string.saved), Toast.LENGTH_SHORT).show();
                                            name.setText(newName);
                                            dialog.dismiss();
                                        } else
                                            Toast.makeText(ctx, ctx.getString(R.string.error), Toast.LENGTH_SHORT).show();
                                    }
                                }.execute(name.getText().toString(), newName);
                            }
                            else Toast.makeText(ctx, ctx.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                        }
                    });
                    dialog.show();
                }
            });
            deleteButton = (Button) itemView.findViewById(R.id.delete_button);
            deleteButtonListener = new DeleteButtonListener();
            deleteButton.setOnClickListener(deleteButtonListener);
        }
    }

    public void notifyChanges(List<String> namesList) {
        this.namesList = namesList;
        notifyDataSetChanged();
        Log.d(TAG, "namesList.size " + namesList.size());
    }

    private class DeleteButtonListener implements View.OnClickListener {
        private String name;

        @Override
        public void onClick(View v) {
            delete(name);
        }

        public void setName(String name) {
            this.name = name;
        }
    }

    private void delete(final String name) {
        if(Utils.isNetworkConnected(ctx)) {
            new DeleteName() {
                @Override
                public void onPostExecute(Boolean deleted) {
                    if (deleted) {
                        int position = namesList.indexOf(name);
                        namesList.remove(position);
                        NamesOps.saveNames(ctx, namesList);
                        notifyDataSetChanged();
                        notifyItemRemoved(position);
                    } else
                        Toast.makeText(ctx, ctx.getString(R.string.error), Toast.LENGTH_SHORT).show();
                }
            }.execute(name);
        }
        else Toast.makeText(ctx, ctx.getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
    }

}