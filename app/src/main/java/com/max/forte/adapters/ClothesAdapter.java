package com.max.forte.adapters;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.max.forte.R;
import com.max.forte.model.Clothes;

import java.util.List;

public class ClothesAdapter extends RecyclerView.Adapter<ClothesAdapter.ViewHolder> {

    final String TAG = "ClothesAdapter";
    private List<Clothes> clothesList;
    private Context ctx;
    private int position;

    private boolean groupsVisible;
    private static final boolean debug = false;

    public ClothesAdapter(List<Clothes> clothesList, Context ctx, boolean groupsVisible) {
        if(debug)
            Log.d(TAG, "" + groupsVisible);
        this.clothesList = clothesList;
        this.ctx = ctx;
        this.groupsVisible = groupsVisible;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.clothes_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        Clothes clothes = clothesList.get(position);
        if (groupsVisible)
            holder.name.setText(clothes.getName());
        holder.model.setText("������:                     " + clothes.getModel());
        if(debug)
            Log.d(TAG, ("������:                     " + clothes.getModel()) + "");
        holder.info.setText("\n�������:                      ");
        holder.info.append(clothes.getSizesStroke());
        holder.info.append("\n����������                  ");
        holder.info.append(clothes.getQuantityStroke());
        holder.info.append(
                        "\n���� �����������      " + clothes.getEntranceDate() +
                        "\n�����:      " + clothes.getBuyPrice() +
                        "\n�������:      " + clothes.getSalePrice());
        String marks = clothesList.get(position).getMarks();
        if (marks != null && !marks.equals("")) {
            holder.markIcon.setVisibility(View.VISIBLE);
            holder.markIcon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showMarks(position);
                }
            });
        }
    }

    void showMarks(int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
        builder.setTitle(R.string.marks)
                .setMessage(clothesList.get(position).getMarks())
                .setCancelable(true)
                .setPositiveButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    public int getItemCount() {
        return clothesList.size();
    }

    public void notifyChanges(List<Clothes> clothesList) {
        this.clothesList = clothesList;
        notifyDataSetChanged();
    }

    protected class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        public TextView name, model, info;
        public ImageButton markIcon;

        public ViewHolder(View itemView) {
            super(itemView);
            itemView.setOnCreateContextMenuListener(this);
            if (groupsVisible) {
                name = (TextView) itemView.findViewById(R.id.name);
                name.setVisibility(View.VISIBLE);
            }
            model = (TextView) itemView.findViewById(R.id.clothes_name);
            info = (TextView) itemView.findViewById(R.id.clothes_info);
            markIcon = (ImageButton) itemView.findViewById(R.id.mark_icon);
            model.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.d(TAG, "name click");
                }
            });
            itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    setPosition(getAdapterPosition());
                    if(debug)
                        Log.d(TAG, "position " + getAdapterPosition());
                    return false;
                }
            });
        }

        @Override
        public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
            menu.add(0, 0, 0, "��������");//nameId, itemId, order, title
            menu.add(0, 1, 0, "�������");
        }
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

}