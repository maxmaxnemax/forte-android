package com.max.forte;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class Utils {

    public static int numberOfDaysInMonth(int month) {
        switch (month) {
            case 3:
            case 5:
            case 8:
            case 10:
                return 30;
            case 1:
                return 28;
            default:
                return 31;

        }
    }

    public static boolean isNetworkConnected(Context ctx) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) ctx.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

}
