package com.max.forte.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.max.forte.MainActivity;
import com.max.forte.R;
import com.max.forte.Utils;
import com.max.forte.adapters.SalesAdapter;
import com.max.forte.asyncTasks.DeleteSale;
import com.max.forte.asyncTasks.GetSales;
import com.max.forte.model.Clothes;
import com.max.forte.model.Sale;
import com.max.forte.data_ops.ClothesOps;
import com.max.forte.data_ops.SalesOps;
import com.max.forte.view.RecView;
import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SalesFragment extends Fragment {

    private static final String TAG = "SalesFragment";
    private static final String SALES_BUNDLE_KEY = "SALES_KEY", FILTER_SALE_DATE_TAG = "filterSalesBySaleDate", SORT_DOWN_TAG = "sortDownSales";
    private ArrayList<Sale> salesList;
    private SalesAdapter salesAdapter;
    private RecView recyclerView;

    public static String title, id;
    ProgressBar pb;
    private String chosenName, saleDateFilter;
    private boolean sortDown, groupsVisible = true;
    private static boolean debug = false;

    public SalesFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (debug)
            Log.d(TAG, "onCreate");
        setHasOptionsMenu(true);
        setRetainInstance(true);
    }

    public static SalesFragment newInstance(String chosenName) {
        Log.d(TAG, "newInstance " + chosenName);
        final SalesFragment fragment = new SalesFragment();
        final Bundle args = new Bundle();
        args.putString("name", chosenName);
        fragment.setArguments(args);
        return fragment;
    }

    View view;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        if (debug)
            Log.d(TAG, "onCreateView");

        if (view == null) {

            view = inflater.inflate(R.layout.clothes_fragment, container, false);
            recyclerView = (RecView) view.findViewById(R.id.rec_view);
            RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setItemAnimator(itemAnimator);
            recyclerView.setEmptyView(view.findViewById(R.id.empty));
            registerForContextMenu(recyclerView);

            FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
            fab.attachToRecyclerView(recyclerView);
            fab.setShadow(true);
            fab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ((MainActivity) getActivity()).addSale(chosenName);
                }
            });
            SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
            saleDateFilter = sp.getString(FILTER_SALE_DATE_TAG, "day");
            sortDown = sp.getBoolean(SORT_DOWN_TAG, true);

            if (savedInstanceState != null) {   // restore the playlist after an orientation change
                if (debug)
                    Log.d(TAG, "savedInstateState != null");
                salesList = savedInstanceState.getParcelableArrayList(SALES_BUNDLE_KEY);
                ((MainActivity) getActivity()).setSalesList(salesList);
            } else {
                salesList = ((MainActivity) getActivity()).getSalesList();
                Bundle args = getArguments();
                if (args != null && null != args.getString("name")) {
                    chosenName = args.getString("name");
                    String toolbarTitle = getString(R.string.sales) + ": " + chosenName;
                    getActivity().setTitle(toolbarTitle);
                }
            }

            if (salesList != null) {   // ensure the adapter and listview are initialized
                if (debug)
                    Log.d(TAG, "salesList != null");
                filterSalesList(chosenName);
                sortSalesList();
                initListAdapter();
            } else
                getSales();

        }// else ((MainActivity) getActivity()).fillDrawer(NamesOps.getNames(getActivity()));
        return view;
    }

    private void getSales() {
        if (Utils.isNetworkConnected(getActivity())) {
            new GetSales(saleDateFilter) {
                @Override
                public void onPostExecute(ArrayList<Sale> result) {
                    if (result != null) {
                        salesList = result;
                        if(isAdded())
                            ((MainActivity) getActivity()).setSalesList(salesList);
                        filterSalesList(chosenName);
                        sortSalesList();
                        initListAdapter();
                    }
                }
            }.execute();
        } else {
            salesList = SalesOps.getSales(getActivity());
            ((MainActivity) getActivity()).setSalesList(salesList);
            filterSalesList(chosenName);
            sortSalesList();
            initListAdapter();
        }
    }

    public void filterSalesList(String chosenName) {
        if (debug)
            Log.d(TAG, "filterSalesList " + chosenName);
        this.chosenName = chosenName;
        salesList = ((MainActivity) getActivity()).getSalesList();
        ArrayList<Sale> chosenNameSalesList = new ArrayList<>();
        for (Sale s : salesList) {
            if (chosenName != null) {   //  filter by name
                if (debug)
                    Log.d(TAG, "s " + s.getModel());
                if (chosenName.equals(s.getName()) && s.isInRange(saleDateFilter)) {
                    if (debug)
                        Log.d(TAG, "equals " + chosenName);
                    chosenNameSalesList.add(s);
                }
            } else if (s.isInRange(saleDateFilter))
                chosenNameSalesList.add(s);
        }
        salesList = chosenNameSalesList;
    }

    public void notifyAdapter() {    //  on drawerClick
        if (salesAdapter == null || groupsVisible) {
            groupsVisible = false;
            initListAdapter();
        }
        else
            salesAdapter.notifyChanges(salesList);
    }

    public void notifyChanges() {   //  due to show/not name name
        if (debug)
            Log.d(TAG, "notifyChangesAfter ");
        filterSalesList(chosenName);
        sortSalesList();
        if (salesAdapter == null)
            initListAdapter();
        else
            salesAdapter.notifyChanges(salesList);
    }

    private void initListAdapter() {
        if (debug)
            Log.d(TAG, "set adapter");
        salesAdapter = new SalesAdapter(salesList, getActivity(), groupsVisible);
        recyclerView.setAdapter(salesAdapter);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        if (debug)
            Log.d(TAG, "onPrep");

        if (sortDown) {
            menu.findItem(R.id.sort_up_down).setIcon(R.drawable.ic_arrow_down);
            menu.findItem(R.id.sort_up_down).setTitle(getString(R.string.sort_down));
        } else {
            menu.findItem(R.id.sort_up_down).setIcon(R.drawable.ic_arrow_up);
            menu.findItem(R.id.sort_up_down).setTitle(getString(R.string.sort_up));
        }
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        if (debug)
            Log.d(TAG, "onCreate menu");
        inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_sales_fragment, menu);
    }

    public boolean onContextItemSelected(MenuItem item) {
        if (getUserVisibleHint()) {
            final int position;
            try {
                position = salesAdapter.getPosition();
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage(), e);
                return super.onContextItemSelected(item);
            }
            Log.d(TAG, "delete position " + position);
            switch (item.getItemId()) {
                case 0:
                    ((MainActivity) getActivity()).changeSale(salesList.get(position));    // change sale
                    break;
                case 1: //  cancel sale
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.cancel_sale)
                            .setMessage(getActivity().getString(R.string.cancel_sale) + "?")
                            .setCancelable(true)
                            .setNegativeButton(R.string.no,
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    })
                            .setPositiveButton(R.string.ok,
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            if (Utils.isNetworkConnected(getActivity())) {
                                                Sale sale = salesList.get(position);
                                                Clothes clothes = null;
                                                Log.d(TAG, "sale.getClothesId() " + sale.getClothesId());
                                                for (Clothes c : ((MainActivity) getActivity()).getClothesList()) {
                                                    Log.d(TAG, "c.getId() " + c.getId());
                                                    if (c.getId() == sale.getClothesId()) {
                                                        clothes = c;
                                                        Log.d(TAG, "clothes " + clothes.getId());
                                                        break;
                                                    }
                                                }
                                                if (sale.getSizes() != null && sale.getSizes().length() != 0) {
                                                    List<String> sizesNotSold = clothes.getSizesNotSold();
                                                    List<String> sizesSold = clothes.getSizesSold();
                                                    for (String sizeToCancel : sale.getSizesList()) {
                                                        sizesNotSold.add(sizeToCancel);
                                                        clothes.setQuantityNotSold(clothes.getQuantityNotSold() + 1);
                                                        sizesSold.remove(sizeToCancel);
                                                    }
                                                    clothes.setSizesNotSold(sizesNotSold);
                                                    clothes.setSizesSold(sizesSold);
                                                } else
                                                    clothes.setQuantityNotSold(clothes.getQuantityNotSold() + sale.getQuantity());
                                                final Clothes finalClothesToCancelSale = clothes;
                                                new DeleteSale() {
                                                    @Override
                                                    public void onPostExecute(Boolean deleted) {
                                                        if (deleted && isAdded()) {
                                                            ArrayList<Clothes> clothesList = ((MainActivity) getActivity()).getClothesList();
                                                            clothesList.set(clothesList.indexOf(finalClothesToCancelSale), finalClothesToCancelSale);
                                                            ClothesOps.saveClothes(getActivity(), clothesList);
                                                            ((MainActivity) getActivity()).setClothesList(clothesList);
                                                            ((MainActivity) getActivity()).notifyClothesChanges();

                                                            salesList.remove(position);
                                                            SalesOps.saveSales(getActivity(), salesList);
                                                            ((MainActivity) getActivity()).setSalesList(salesList);
                                                            salesAdapter.notifyChanges(salesList);
                                                        } else
                                                            Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                                                    }
                                                }.execute(new Pair(salesList.get(position), clothes));
                                            } else
                                                Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                                        }
                                    })
                            .create().show();
                    break;
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sort_up_down:
                if (debug)
                    Log.d(TAG, "sort_up_down:");
                sortUpDown(item);
                return true;
            case R.id.filterBySaleDate:
                if (debug)
                    Log.d(TAG, "filter");
                showFilterBySaleDateDialog();
                return true;
            case R.id.refresh:
                Log.d(TAG, "refresh");
                refresh();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void sortUpDown(MenuItem item) {
        if (sortDown) {
            item.setIcon(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up));
        } else
            item.setIcon(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down));
        sortDown = !sortDown;
        sortSalesList();
        salesAdapter.notifyChanges(salesList);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        prefs.edit().putBoolean(SORT_DOWN_TAG, sortDown).apply();
    }

    void showFilterBySaleDateDialog() {
        final AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        final String filters[] = getResources().getStringArray(R.array.time_filters);
        final String filtersShowing[] = getResources().getStringArray(R.array.time_filters_showing);
        int checkedPosition = filters.length - 1;
        Log.d(TAG, "showFilterBySaleDateDialog " + checkedPosition);
        if (saleDateFilter != null && !saleDateFilter.equals(getString(R.string.all_time))) {
            for (int i = 0; i < filters.length; i++)
                if (filters[i].equals(saleDateFilter)) {
                    checkedPosition = i;
                    break;
                }
        }
        final int lastPosition = checkedPosition;
        adb.setSingleChoiceItems(filtersShowing, checkedPosition, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface d, int i) {
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor e = sp.edit();
                saleDateFilter = filters[i];
                e.putString(FILTER_SALE_DATE_TAG, saleDateFilter).apply();
                if(lastPosition > i)
                    notifyChanges();
                else
                    getSales();
                d.dismiss();
            }
        }).setNegativeButton("������", null).setTitle("������� ��:").show();
    }

    void refresh() {
        getActivity().setTitle(getString(R.string.sales));
        salesList = ((MainActivity) getActivity()).getSalesList();
        filterSalesList(null);
        sortSalesList();
        if (!groupsVisible) {
            groupsVisible = true;
            initListAdapter();
        } else
            salesAdapter.notifyChanges(salesList);
        chosenName = null;
    }

    void sortSalesList() {
        if (debug)
            Log.d(TAG, "sortSalesList " + sortDown);
        if (sortDown)
            Collections.sort(salesList);
        else
            Collections.reverse(salesList);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (debug)
            Log.d(TAG, "onResume");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //noInternet = !Internet.isNetworkConnected(getActivity());
    }

    public void onDestroyView() {
        super.onDestroyView();
        if (debug)
            Log.d(TAG, "onDestroyView");
        salesAdapter = null;
        pb = null;
    }

    public void onDestroy() {
        super.onDestroy();
        if (debug)
            Log.d(TAG, "onDestroy");
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(SALES_BUNDLE_KEY, salesList);
        super.onSaveInstanceState(outState);
    }
}
