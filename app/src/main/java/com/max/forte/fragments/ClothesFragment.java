package com.max.forte.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.max.forte.R;
import com.max.forte.MainActivity;
import com.max.forte.Utils;
import com.max.forte.adapters.ClothesAdapter;
import com.max.forte.asyncTasks.DeleteClothes;
import com.max.forte.asyncTasks.GetClothes;
import com.max.forte.model.Clothes;
import com.max.forte.data_ops.ClothesOps;
import com.max.forte.view.RecView;
import com.melnykov.fab.FloatingActionButton;

import java.util.ArrayList;
import java.util.Collections;

public class ClothesFragment extends Fragment {

    private static String TAG = "ClothesFragment";
    private ArrayList<Clothes> clothesList;
    ClothesAdapter clothesAdapter;

    RecView recyclerView;
    ProgressBar pb;
    private String chosenName, entranceDateFilter;
    private boolean showSoldClothes, sortDown, groupsVisible = true;  //  sort from the earliest to the oldest by entranceDate
    private static final String FILTER_SOLD_TAG = "showSoldClothes", FILTER_ENTRANCE_DATE_TAG = "filterClothesByEntranceDate", SORT_DOWN_TAG = "sortDownClothes", CLOTHES_BUNDLE_KEY = "CLOTHES_KEY";
    private static final boolean debug = false;

    public ClothesFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (debug)
            Log.d(TAG, "onCreate");
        setHasOptionsMenu(true);
        setRetainInstance(true);
    }

    public static ClothesFragment newInstance(String chosenName) {
        if (debug)
            Log.d(TAG, "newInstance " + chosenName);
        final ClothesFragment fragment = new ClothesFragment();
        final Bundle args = new Bundle();
        args.putString("name", chosenName);
        fragment.setArguments(args);
        return fragment;
    }

    View view;

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {
        if (debug)
            Log.d(TAG, "onCreateView");

        //getActivity().setTitle(getString(R.string.app_name));
        view = inflater.inflate(R.layout.clothes_fragment, container, false);
        recyclerView = (RecView) view.findViewById(R.id.rec_view);
        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity())); recyclerView.setItemAnimator(itemAnimator); recyclerView.setEmptyView(view.findViewById(R.id.empty));
        registerForContextMenu(recyclerView);

        FloatingActionButton fab = (FloatingActionButton) view.findViewById(R.id.fab);
        fab.attachToRecyclerView(recyclerView); fab.setShadow(true);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (debug)
                    Log.d(TAG, "chosenName " + chosenName);
                ((MainActivity) getActivity()).addClothes(chosenName);
            }
        });
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
        entranceDateFilter = sp.getString(FILTER_ENTRANCE_DATE_TAG, null);
        showSoldClothes = sp.getBoolean(FILTER_SOLD_TAG, true);
        sortDown = sp.getBoolean(SORT_DOWN_TAG, true);

        if (savedInstanceState != null) {   // restore the list after an orientation change
            if (debug)
                Log.d(TAG, "savedInstateState != null");
            clothesList = savedInstanceState.getParcelableArrayList(CLOTHES_BUNDLE_KEY);
            ((MainActivity) getActivity()).setClothesList(clothesList);
        } else {
            clothesList = ((MainActivity) getActivity()).getClothesList();
            Bundle args = getArguments();
            if (args != null && null != args.getString("name")) {
                if(debug)
                    Log.d(TAG, "onCreateView, " + "clothesList null? " + (clothesList == null));
                chosenName = args.getString("name");
                getActivity().setTitle(chosenName);
            }
        }

        if (clothesList != null) {   // ensure the adapter and listview are initialized
            if (debug)
                Log.d(TAG, "clothesList != null");
            filterClothesList(chosenName);
            sortClothesList();
            initListAdapter();
        } else {
            getClothes();
        }
        return view;
    }

    private void getClothes() {
        if(Utils.isNetworkConnected(getActivity())) {
            new GetClothes() {
                @Override
                public void onPostExecute(ArrayList<Clothes> result) {
                    if (result != null) {
                        clothesList = result;
                        if(isAdded())
                            ((MainActivity) getActivity()).setClothesList(clothesList);
                        filterClothesList(chosenName);
                        sortClothesList();
                        initListAdapter();
                    }
                }
            }.execute();
        }
        else{
            clothesList = ClothesOps.getClothes(getActivity());
            ((MainActivity) getActivity()).setClothesList(clothesList);
            filterClothesList(chosenName);
            sortClothesList();
            initListAdapter();
        }
    }

    public void filterClothesList(String chosenName) {    //  filter by name, sold, entranceDate
        if(debug)
            Log.d(TAG, "filterClothesList " + chosenName);
        this.chosenName = chosenName;
        clothesList = ((MainActivity) getActivity()).getClothesList();
        ArrayList<Clothes> chosenNameClothesList = new ArrayList<>();
        for (Clothes c : clothesList) {
            if (debug)
                Log.d(TAG, "c " + c.getModel());
            if (chosenName != null) {
                if (chosenName.equals(c.getName())) {
                    if (!showSoldClothes) {   //  �� ���������� ���������
                        if (c.areNotSoldClothes() && c.isInRange(entranceDateFilter))  //  ���� ������ �� �������
                            chosenNameClothesList.add(c);
                    } else if (c.isInRange(entranceDateFilter))    //  ���������� ��� ������ ������
                        chosenNameClothesList.add(c);
                }
            } else {
                if (!showSoldClothes) {   //  �� ���������� ���������
                    if (c.areNotSoldClothes() && c.isInRange(entranceDateFilter))  //  ���� ������ �� �������
                        chosenNameClothesList.add(c);
                } else if (c.isInRange(entranceDateFilter))   //  ���������� ��� ������ ������
                    chosenNameClothesList.add(c);
            }
        }
        clothesList = chosenNameClothesList;
    }

    public void notifyAdapter(){    //  on drawerClick
        if (clothesAdapter == null || groupsVisible) {
            groupsVisible = false;
            initListAdapter();
        }
        else
            clothesAdapter.notifyChanges(clothesList);
    }

    public void notifyChanges() {
        if (debug)
            Log.d(TAG, "notifyChangesAfter ");
        filterClothesList(chosenName);    //  ���� �� ������� ������, �� ������� ���������, ���� �����
        sortClothesList();
        if (clothesAdapter == null)
            initListAdapter();
        else
            clothesAdapter.notifyChanges(clothesList);
    }

    private void initListAdapter() {
        if (debug)
            Log.d(TAG, "set adapter");
        clothesAdapter = new ClothesAdapter(clothesList, getActivity(), groupsVisible);
        recyclerView.setAdapter(clothesAdapter);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (debug)
            Log.d(TAG, "onPrep");

        if (sortDown) {
            menu.findItem(R.id.sort_up_down).setIcon(R.drawable.ic_arrow_down);
            menu.findItem(R.id.sort_up_down).setTitle(getString(R.string.sort_down));
        } else {
            menu.findItem(R.id.sort_up_down).setIcon(R.drawable.ic_arrow_up);
            menu.findItem(R.id.sort_up_down).setTitle(getString(R.string.sort_up));
        }

        if (showSoldClothes)
            menu.findItem(R.id.showSoldClothes).setIcon(ContextCompat.getDrawable(getActivity(), R.drawable.ic_checkbox_on));
        else
            menu.findItem(R.id.showSoldClothes).setIcon(ContextCompat.getDrawable(getActivity(), R.drawable.ic_checkbox_off));
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_clothes_fragment, menu);
    }

    public boolean onContextItemSelected(MenuItem item) {
        if (getUserVisibleHint()) {
            final int position;
            try {
                position = clothesAdapter.getPosition();
            } catch (Exception e) {
                Log.d(TAG, e.getLocalizedMessage(), e);
                return super.onContextItemSelected(item);
            }
            if (debug)
                Log.d(TAG, "delete position " + position);
            switch (item.getItemId()) {
                case 0:
                    ((MainActivity) getActivity()).changeClothes(clothesList.get(position));    // change clothes
                    break;
                case 1: //  delete clothes
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(R.string.delete_clothes_title)
                            .setMessage(getActivity().getString(R.string.delete_clothes))
                            .setCancelable(true)
                            .setNegativeButton(R.string.no,
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    })
                            .setPositiveButton(R.string.ok,
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(final DialogInterface dialog, int id) {
                                            if(Utils.isNetworkConnected(getActivity())) {
                                                new DeleteClothes() {
                                                    @Override
                                                    public void onPostExecute(Boolean deleted) {
                                                        if (deleted) {
                                                            clothesList.remove(position);
                                                            if(isAdded()) {
                                                                ClothesOps.saveClothes(getActivity(), clothesList);
                                                                ((MainActivity) getActivity()).setClothesList(clothesList);
                                                            }
                                                            clothesAdapter.notifyChanges(clothesList);
                                                            dialog.dismiss();
                                                        } else
                                                            Toast.makeText(getActivity(), getString(R.string.error), Toast.LENGTH_SHORT).show();
                                                    }
                                                }.execute(clothesList.get(position).getId());
                                            }
                                            else Toast.makeText(getActivity(), getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                                        }
                                    })
                            .create().show();
                    break;
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.sort_up_down:
                if(debug)
                    Log.d(TAG, "sort_up_down:");
                sortUpDown(item);
                return true;
            case R.id.filterByEntranceDate:
                if(debug)
                    Log.d(TAG, "filterByEntranceDate clicked");
                showFilterByEntranceDateDialog();
                return true;
            case R.id.showSoldClothes:
                if(debug)
                    Log.d(TAG, "showSoldClothes clicked");
                if (showSoldClothes)
                    item.setIcon(ContextCompat.getDrawable(getActivity(), R.drawable.ic_checkbox_off));
                else
                    item.setIcon(ContextCompat.getDrawable(getActivity(), R.drawable.ic_checkbox_on));
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
                showSoldClothes = !showSoldClothes;
                prefs.edit().putBoolean(FILTER_SOLD_TAG, showSoldClothes).apply();
                filterClothesList(chosenName);
                clothesAdapter.notifyChanges(clothesList);
                return true;
            case R.id.refresh:
                if(debug)
                    Log.d(TAG, "refresh");
                refresh();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void sortUpDown(MenuItem item) {
        if (sortDown) {
            item.setIcon(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_up));
        } else
            item.setIcon(ContextCompat.getDrawable(getActivity(), R.drawable.ic_arrow_down));
        sortDown = !sortDown;
        sortClothesList();
        clothesAdapter.notifyChanges(clothesList);
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getActivity());
        prefs.edit().putBoolean(SORT_DOWN_TAG, sortDown).apply();
    }

    void showFilterByEntranceDateDialog() {
        final AlertDialog.Builder adb = new AlertDialog.Builder(getActivity());
        final String filters[] = getResources().getStringArray(R.array.time_filters);
        final String filtersShowing[] = getResources().getStringArray(R.array.time_filters_showing);
        int checkedPosition = filters.length - 1;
        if(debug)
            Log.d(TAG, "showFilterByEntranceDateDialog " + checkedPosition);
        if (entranceDateFilter != null && !entranceDateFilter.equals(getString(R.string.all_time))) {
            for (int i = 0; i < filters.length; i++)
                if (filters[i].equals(entranceDateFilter)) {
                    checkedPosition = i;
                    break;
                }
        }
        final int lastPosition = checkedPosition;
        adb.setSingleChoiceItems(filtersShowing, checkedPosition, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface d, int i) {
                SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(getActivity());
                SharedPreferences.Editor e = sp.edit();
                entranceDateFilter = filters[i];
                e.putString(FILTER_ENTRANCE_DATE_TAG, entranceDateFilter).apply();
                if(lastPosition > i)
                    notifyChanges();
                else
                    getClothes();
                if(debug)
                    Log.d(TAG, "selector click");
                d.dismiss();
            }

        }).setNegativeButton("������", null).setTitle("���� ����������� ��:").show();
    }

    void refresh() {
        if (debug)
            Log.d(TAG, "show clothes " + groupsVisible);
        getActivity().setTitle(getString(R.string.app_name));
        clothesList = ((MainActivity) getActivity()).getClothesList();
        filterClothesList(null);
        sortClothesList();
        if(!groupsVisible) {
            groupsVisible = true;
            initListAdapter();
        }
        else
            clothesAdapter.notifyChanges(clothesList);
        chosenName = null;
    }

    void sortClothesList() {
        if(debug)
            Log.d(TAG, "sortClothesList " + sortDown);
        if (sortDown)
            Collections.sort(clothesList);
        else
            Collections.reverse(clothesList);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (debug)
            Log.d(TAG, "onResume");
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //noInternet = !Internet.isNetworkConnected(getActivity());
    }

    public void onDestroyView() {
        super.onDestroyView();
        if (debug)
            Log.d(TAG, "onDestroyView");
        clothesAdapter = null;
        pb = null;
    }

    public void onDestroy() {
        super.onDestroy();
        if (debug)
            Log.d(TAG, "onDestroy");
    }

    public void onSaveInstanceState(Bundle outState) {
        outState.putParcelableArrayList(CLOTHES_BUNDLE_KEY, clothesList);
        super.onSaveInstanceState(outState);
    }

}
