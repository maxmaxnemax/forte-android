package com.max.forte;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.max.forte.activities.AddClothesActivity;
import com.max.forte.activities.AddSaleActivity;
import com.max.forte.activities.ChangeClothesActivity;
import com.max.forte.activities.ChangeSaleActivity;
import com.max.forte.activities.NamesActivity;
import com.max.forte.adapters.PagerAdapter;
import com.max.forte.asyncTasks.GetNames;
import com.max.forte.asyncTasks.PostName;
import com.max.forte.model.Clothes;
import com.max.forte.model.Sale;
import com.max.forte.data_ops.NamesOps;
import com.max.forte.view.SlidingTabLayout;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "MA";
    private static final String NAMES_BUNDLE_KEY = "NAMES_KEY";
    private static final String CLOTHES_BUNDLE_KEY = "CLOTHES_KEY";

    ArrayList<Clothes> clothesList;
    ArrayList<Sale> salesList;

    private ActionBarDrawerToggle drawerToggle;
    private DrawerLayout drawer;
    private ListView drawerList;
    FrameLayout drawerView;

    private ArrayList<String> namesList = new ArrayList<>();
    ArrayAdapter<String> namesAdapter;

    Context ctx = this;
    WeakReference<Context> cReference;

    ViewPager pager;
    PagerAdapter pagerAdapter;

    String currentTitleAtPos0, currentTitleAtPos1;
    private static final boolean debug = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        Log.d(TAG, "onCreate");
        cReference = new WeakReference<>(ctx);
        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null)
            namesList = new ArrayList<>();
        else {
            namesList = savedInstanceState.getStringArrayList(NAMES_BUNDLE_KEY);
            clothesList = savedInstanceState.getParcelableArrayList(CLOTHES_BUNDLE_KEY);
        }

        for (String g : namesList)
            Log.d(TAG, "g.toStr " + g);

        setUpDrawer(toolbar);
        getNames();
        if (savedInstanceState == null) {
            if (debug)
                Log.d(TAG, "onCreate saved == null");
            pager = (ViewPager) findViewById(R.id.viewpager);
            pagerAdapter = new PagerAdapter(getSupportFragmentManager(), ctx);
            pager.setAdapter(pagerAdapter);

        } else {
            if (debug)
                Log.d(TAG, "onCreate saved ne null");
            getSupportActionBar().setDisplayShowHomeEnabled(false);
            getSupportActionBar().setTitle(R.string.app_name);
        }
        SlidingTabLayout slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setViewPager(pager);
        slidingTabLayout.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if(debug)
                    Log.d(TAG, "onPageSelected " + position);
                if (position == 0) {
                    if (currentTitleAtPos0 == null)
                        currentTitleAtPos0 = getString(R.string.app_name);
                    getSupportActionBar().setTitle(currentTitleAtPos0);
                } else if (position == 1) {
                    if (currentTitleAtPos1 == null)
                        currentTitleAtPos1 = getString(R.string.sales);
                    getSupportActionBar().setTitle(currentTitleAtPos1);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

    }

    private void getNames(){
        if(Utils.isNetworkConnected(ctx)) {
            new GetNames() {
                @Override
                public void onPostExecute(ArrayList<String> result) {
                    if (result != null) {
                        namesList = result;
                        NamesOps.saveNames(ctx, namesList); //  save in local
                    }
                    fillDrawer(namesList);
                }
            }.execute();
        }
        else{
            NamesOps.getNames(ctx);
            fillDrawer(namesList);
        }
    }

    protected void onNewIntent(Intent intent) {
        Log.d(TAG, "onNewIntent");
        namesList = NamesOps.getNames(ctx);
        if (namesList != null) {
            namesAdapter = new ArrayAdapter(ctx, R.layout.drawer_list_item, namesList);
            drawerList.setAdapter(namesAdapter);
        }
    }

    public void fillDrawer(ArrayList<String> namesList) {   //  when from internet
        if(debug)
            Log.d(TAG, "fillDrawer");
        if (namesList != null && namesList.size() != 0) {
            this.namesList = namesList;
            if(debug)
                Log.d(TAG, "drawerAdapter == null " + (namesAdapter == null));
            initDrawerAdapter(namesList);
        }
    }

    public void changeClothes(Clothes clothes) {
        if (namesList == null || namesList.size() == 0)
            addName(true); //  redirect after
        else {
            Intent intent = new Intent(ctx, ChangeClothesActivity.class);
            intent.putParcelableArrayListExtra("clothesList", clothesList);
            intent.putExtra("clothes", clothes);
            intent.putExtra("position", clothesList.indexOf(clothes));
            startActivityForResult(intent, 1);
        }
    }

    public void addClothes(String chosenName) {
        if (namesList == null || namesList.size() == 0)
            addName(true); //  redirect after
        else {
            if (clothesList == null)
                clothesList = new ArrayList<>();
            Intent intent = new Intent(ctx, AddClothesActivity.class);
            intent.putParcelableArrayListExtra("clothesList", clothesList);
            if (chosenName != null)
                intent.putExtra("name", chosenName);
            startActivityForResult(intent, 1);
        }
    }

    public void addSale(String chosenName) {
        if (namesList == null || namesList.size() == 0)
            addName(true); //  redirect after
        else {
            if (salesList == null)
                salesList = new ArrayList<>();
            if (clothesList == null)
                clothesList = new ArrayList<>();
            Intent intent = new Intent(ctx, AddSaleActivity.class);
            intent.putParcelableArrayListExtra("clothesList", clothesList);
            intent.putParcelableArrayListExtra("salesList", salesList);
            if (chosenName != null)
                intent.putExtra("name", chosenName);
            startActivityForResult(intent, 2);
        }
    }

    public void changeSale(Sale sale) {
        Intent intent = new Intent(ctx, ChangeSaleActivity.class);
        intent.putParcelableArrayListExtra("clothesList", clothesList);
        intent.putParcelableArrayListExtra("salesList", salesList);
        intent.putExtra("sale", sale);
        startActivityForResult(intent, 2);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d(TAG, "onActivityResult " + requestCode);
        if (data == null)
            return;
        clothesList = data.getParcelableArrayListExtra("clothesList");
        setClothesList(clothesList);
        if (requestCode == 2) {
            salesList = data.getParcelableArrayListExtra("salesList");
            setSalesList(salesList);
            for (Sale s : salesList)
                Log.d(TAG, "s: " + s);
            pagerAdapter.notifySalesChanges();
        }
        pagerAdapter.notifyClothesChanges();
    }

    public void addName(final boolean redirectAfter) { //  0   -   clothes, 1  -   sales
        final EditText input_name = new EditText(this);
        input_name.setInputType(InputType.TYPE_CLASS_TEXT);
        new AlertDialog.Builder(this)
                .setTitle(getString(R.string.add_to_names))
                .setMessage(getString(R.string.input_name))
                .setView(input_name)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        if (Utils.isNetworkConnected(ctx)) {
                            final String name = input_name.getText().toString().replaceAll("[\\\\p{Alphabetic} ]*", "");
                            input_name.setText("");
                            if (namesList == null)
                                namesList = new ArrayList<>();
                            if (NamesOps.check(ctx, namesList, name)) {
                                new PostName() {
                                    @Override
                                    public void onPostExecute(Boolean added) {
                                        if (added) {
                                            namesList = NamesOps.addName(ctx, namesList, name); //  save in local
                                            fillDrawer(namesList);
                                            if (namesAdapter == null)
                                                initDrawerAdapter(namesList);
                                            if (redirectAfter)
                                                addClothes(null);
                                        } else
                                            Toast.makeText(ctx, getString(R.string.error), Toast.LENGTH_SHORT).show();
                                    }
                                }.execute(name);
                            }
                        }
                        else Toast.makeText(ctx, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                    }
                }).setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.dismiss();
            }
        }).show();
    }

    private void setUpDrawer(Toolbar toolbar) {
        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerList = (ListView) findViewById(R.id.drawer_list);

        drawerView = (FrameLayout) findViewById(R.id.drawer_view);
        drawerList.setOnItemClickListener(new DrawerItemClickListener());
        drawer.closeDrawer(drawerView);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        drawerToggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, 0, 0
        ) {

            public void onDrawerClosed(View view) {invalidateOptionsMenu();}

            public void onDrawerOpened(View drawerView) {
                invalidateOptionsMenu();
            }

        };

        drawer.setDrawerListener(drawerToggle);   // Set the drawer toggle as the DrawerListener
        drawer.setFocusableInTouchMode(false);
    }

    void initDrawerAdapter(List<String> namesList) {
        namesAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, namesList);
        drawerList.setAdapter(namesAdapter);
        namesAdapter.notifyDataSetChanged();
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.d(TAG, "onItemClick");
            if (pager.getCurrentItem() == 0)
                pagerAdapter.showClothes(namesList.get(position));
            else
                pagerAdapter.showSales(namesList.get(position));
            setTitle(namesList.get(position));
            drawer.closeDrawer(drawerView);
        }
    }

    public ArrayList<Clothes> getClothesList() {
        return clothesList;
    }

    public void notifyClothesChanges() {
        pagerAdapter.notifyClothesChanges();
    }

    public ArrayList<Sale> getSalesList() {
        return salesList;
    }

    public void setClothesList(ArrayList<Clothes> clothesList) {
        this.clothesList = clothesList;
    }

    public void setSalesList(ArrayList<Sale> salesList) {
        this.salesList = salesList;
    }

    @Override
    public void onBackPressed() {
        if (!drawer.isDrawerOpen(drawerView)) {
            super.onBackPressed();
        } else
            drawer.closeDrawer(drawerView);
    }

    @Override
    public void setTitle(CharSequence title) {
        Log.d(TAG, "setTitle " + title);
        if (pager != null) {
            if (pager.getCurrentItem() == 0)
                currentTitleAtPos0 = title.toString();
            else
                currentTitleAtPos1 = title.toString();
        }
        getSupportActionBar().setTitle(title);
    }

    public void sendMessageToMax() {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[]{"max_max-nemax@mail.ru"});
        i.putExtra(Intent.EXTRA_SUBJECT, "���������� \"�����\"");
        i.putExtra(Intent.EXTRA_TEXT, "����, ");
        try {
            startActivity(Intent.createChooser(i, "��������� ������..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.modify_names:
                Intent intent = new Intent(ctx, NamesActivity.class);
                startActivity(intent);
                return true;
            case R.id.send_message_to_max:
                sendMessageToMax();
                return true;
            case R.id.add_name:
                addName(false);
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putStringArrayList(NAMES_BUNDLE_KEY, namesList);
        outState.putParcelableArrayList(CLOTHES_BUNDLE_KEY, clothesList);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        drawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(debug)
            Log.d(TAG, "onResume");
    }
}
