package com.max.forte.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.google.common.base.Objects;
import com.max.forte.Utils;

import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

public class Sale implements Parcelable, Comparable<Sale> {

    private int id, clothesId;
    private String name, model;
    private String sizes;
    private int quantity, buyPrice, salePrice;
    private CalendarDay entranceDate, saleDate;
    private String marks;

    private static final String TAG = "Sale";

    public Sale(int id, int clothesId, String name, String model, String sizes, int quantity, int buyPrice, int salePrice, CalendarDay entranceDate,
                CalendarDay saleDate, String marks) {
        if (id != -1)
            this.id = id;
        this.clothesId = clothesId;
        this.name = name;
        this.model = model;
        this.sizes = sizes;
        this.quantity = quantity;
        this.buyPrice = buyPrice;
        this.salePrice = salePrice;
        this.entranceDate = entranceDate;
        this.saleDate = saleDate;
        this.marks = marks;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name, model, entranceDate);
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof Sale) {
            final Sale other = (Sale) obj;
            return Objects.equal(name, other.name)
                    && Objects.equal(model, other.model) // special handling for primitives
                    && Objects.equal(entranceDate, other.entranceDate)
                    && Objects.equal(saleDate, other.saleDate)
                    && Objects.equal(salePrice, other.salePrice);
        } else {
            return false;
        }
    }

    public String getModel() {
        return model;
    }

    public String getName() {
        return name;
    }

    public int getClothesId() {
        return clothesId;
    }

    public CalendarDay getEntranceDate() {
        return entranceDate;
    }

    public String getEntranceDateString() {
        return entranceDate != null ? entranceDate.toString() : "";
    }

    public String getEntranceDateStringFormatted() {
        return entranceDate.getString();
    }

    public int getBuyPrice() {
        return buyPrice;
    }

    public String getClothesIdString() {
        return String.valueOf(clothesId);
    }

    public String getBuyPriceString() {
        return String.valueOf(buyPrice);
    }

    public int getId() {
        return id;
    }

    public String getIdString() {
        return String.valueOf(id);
    }

    public int getQuantity() {
        return quantity;
    }

    public int getSalePrice() {
        return salePrice;
    }

    public String getSalePriceString() {
        return String.valueOf(salePrice);
    }

    public String getSizes() {
        return sizes == null ? "" : sizes;
    }

    public List<String> getSizesList() {
        Log.d(TAG, "sizes: " + sizes);
        if (sizes == null)
            return null;
        else
            return Arrays.asList(sizes.split("[^MLX0-9]"));
    }

    public CalendarDay getSaleDate() {
        return saleDate;
    }

    public String getSaleDateString() {
        return saleDate.toString();
    }

    public String getSaleDateStringFormatted() {
        return saleDate.getString();
    }

    public String getMarks() {
        return marks;
    }

    public void setId(int id) {
        this.id = id;
    }

    protected Sale(Parcel in) {
        id = in.readInt();
        clothesId = in.readInt();
        name = in.readString();
        model = in.readString();
        sizes = in.readString();
        quantity = in.readInt();
        buyPrice = in.readInt();
        salePrice = in.readInt();
        entranceDate = (CalendarDay) in.readValue(CalendarDay.class.getClassLoader());
        saleDate = (CalendarDay) in.readValue(CalendarDay.class.getClassLoader());
        marks = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeInt(clothesId);
        dest.writeString(name);
        dest.writeString(model);
        dest.writeString(sizes);
        dest.writeInt(quantity);
        dest.writeInt(buyPrice);
        dest.writeInt(salePrice);
        dest.writeValue(entranceDate);
        dest.writeValue(saleDate);
        dest.writeString(marks);
    }

    @SuppressWarnings("unused")
    public static final Creator<Sale> CREATOR = new Creator<Sale>() {
        @Override
        public Sale createFromParcel(Parcel in) {
            return new Sale(in);
        }

        @Override
        public Sale[] newArray(int size) {
            return new Sale[size];
        }
    };

    @Override
    public int compareTo(Sale sale) {
        if ((sale.getSaleDate().isAfter(saleDate)))
            return 1;
        else
            return -1;
    }

    @Override
    public String toString() {
        return "Sale{" +
                "id=" + id +
                ", clothesId=" + clothesId +
                ", name='" + name + '\'' +
                ", model='" + model + '\'' +
                ", sizes='" + sizes + '\'' +
                ", quantity=" + quantity +
                ", buyPrice=" + buyPrice +
                ", salePrice=" + salePrice +
                ", entranceDate=" + entranceDate +
                ", saleDate=" + saleDate +
                ", marks='" + marks + '\'' +
                '}';
    }

    public boolean isInRange(String saleDateFilter) {
        if (saleDateFilter == null || "alltime".equals(saleDateFilter))
            return true;
        int year, month, day;
        Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH) + 1;
        day = c.get(Calendar.DAY_OF_MONTH);
        switch (saleDateFilter) {
            case "day":
                return saleDate.isInRange
                        (day > 1 ? new CalendarDay(year, month, day - 1) : // day < 7
                                        month > 0 ? new CalendarDay(year, month - 1, day + Utils.numberOfDaysInMonth(month - 1) - 1)
                                                : new CalendarDay(year - 1, 11, 31),
                                new CalendarDay(year, month, day));
            case "week":
                return saleDate.isInRange
                        (day > 7 ? new CalendarDay(year, month, day - 8) : // day < 7
                                        month > 0 ? new CalendarDay(year, month - 1, day + Utils.numberOfDaysInMonth(month - 1) - 7)
                                                : new CalendarDay(year - 1, 11, day + 23),
                                new CalendarDay(year, month, day));
            case "month":
                return saleDate.isInRange
                        (month > 0 ? new CalendarDay(year, month - 1, day) : new CalendarDay(year - 1, 11, day),
                                new CalendarDay(year, month, day));
            case "halfyear":
                return saleDate.isInRange
                        (month > 5 ? new CalendarDay(year, month - 6, day) : new CalendarDay(year - 1, month + 6, day),
                                new CalendarDay(year, month, day));
            case "year":
                return saleDate.isInRange(new CalendarDay(year - 1, month, day),
                        new CalendarDay(year, month, day));
            default:
                return true;
        }
    }
}