package com.max.forte.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.util.Calendar;

public final class CalendarDay implements Parcelable {
    private static final String TAG = "CalendarDay";
    private int year;
    private int month;
    private int day;
    private int size;

    public CalendarDay(int year, int month, int day) {
        this.year = year;
        this.month = month;
        this.day = day;
    }

    public CalendarDay() {   //  current day
        Calendar c = Calendar.getInstance();
        this.year = c.get(Calendar.YEAR);
        this.month = c.get(Calendar.MONTH) + 1;
        this.day = c.get(Calendar.DAY_OF_MONTH);
    }

    public String getString() {
        return year + "-" + (month / 10 >= 1 ? month : "0" + month) + "-" + (day / 10 >= 1 ? day : "0" + day);
    }

    public int getYear() {
        return this.year;
    }

    public int getMonth() {
        return this.month;
    }

    public int getDay() {
        return this.day;
    }

    public boolean isInRange(CalendarDay minDate, CalendarDay maxDate) {
        Log.d(TAG, "this " + this);
        Log.d(TAG, "minDate " + minDate);
        Log.d(TAG, "maxDate " + maxDate);
        return (minDate == null || !minDate.isAfter(this)) && (maxDate == null || !maxDate.isBefore(this));
    }

    public boolean isBefore(CalendarDay other) {
        if (other == null) {
            throw new IllegalArgumentException("other cannot be null");
        } else {
            return this.year == other.year ? (this.month == other.month ? this.day < other.day : this.month < other.month) : this.year < other.year;
        }
    }

    public boolean isAfter(CalendarDay other) { //  or same day
        return other == null || (this.year == other.year ? (this.month == other.month ? this.day >= other.day : this.month > other.month) : this.year > other.year);
    }

    public boolean equals(Object o) {
        if (this == o) {
            return true;
        } else if (o != null && this.getClass() == o.getClass()) {
            CalendarDay that = (CalendarDay) o;
            return this.day == that.day && (this.month == that.month && this.year == that.year);
        } else {
            return false;
        }
    }

    protected CalendarDay(Parcel in) {
        year = in.readInt();
        month = in.readInt();
        day = in.readInt();
        size = in.readInt();
    }

    @Override
    public String toString() {
        return (day / 10 >= 1 ? day : "0" + day) + "." +
                (month / 10 >= 1 ? month : "0" + month) + "." +
                year;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(year);
        dest.writeInt(month);
        dest.writeInt(day);
        dest.writeInt(size);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CalendarDay> CREATOR = new Parcelable.Creator<CalendarDay>() {
        @Override
        public CalendarDay createFromParcel(Parcel in) {
            return new CalendarDay(in);
        }

        @Override
        public CalendarDay[] newArray(int size) {
            return new CalendarDay[size];
        }
    };
}