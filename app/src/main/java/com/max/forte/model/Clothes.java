package com.max.forte.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.style.StrikethroughSpan;
import android.util.Log;

import com.google.common.base.Objects;
import com.max.forte.Utils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Clothes implements Parcelable, Comparable<Clothes> {

    private int id;
    private String name, model;
    private List<String> sizesNotSold, sizesSold;
    private int allQuantity, quantityNotSold, buyPrice, salePrice;
    private CalendarDay entranceDate;
    private String marks;

    private static final String TAG = "Clothes";
    private static final boolean debug = false;

    public Clothes(int id, String name, String model, List<String> sizesNotSold, List<String> sizesSold, int allQuantity,
                   int quantityNotSold, int buyPrice, int salePrice, CalendarDay entranceDate, String marks) {  //  with sizes
        if (id != -1)
            this.id = id;
        this.name = name;
        this.model = model;
        this.sizesNotSold = sizesNotSold;
        this.sizesSold = sizesSold;
        this.allQuantity = allQuantity;
        this.quantityNotSold = quantityNotSold;
        this.buyPrice = buyPrice;
        this.salePrice = salePrice;
        this.entranceDate = entranceDate;
        this.marks = marks;
    }

    public boolean areNotSoldClothes() {
        return quantityNotSold != 0;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(name, model, entranceDate);
    }

    @Override
    public boolean equals(final Object obj) {
        if (obj instanceof Clothes) {
            final Clothes other = (Clothes) obj;
            return Objects.equal(name, other.name)
                    && Objects.equal(model, other.model) // special handling for primitives
                    && Objects.equal(entranceDate, other.entranceDate);
        } else {
            return false;
        }
    }

    public int getId() {
        return id;
    }

    public String getIdString() {
        return String.valueOf(id);
    }

    public String getName() {
        return name;
    }

    public String getModel() {
        return model;
    }

    public String getEntranceDateString() {
        return entranceDate != null ? entranceDate.toString() : "";
    }

    public CalendarDay getEntranceDate() {
        return entranceDate == null ? new CalendarDay(0, 0, 0) : entranceDate;
    }

    public String getEntranceDateStringFormatted() {
        return entranceDate.getString();
    }

    public int getAllQuantity() {
        return allQuantity;
    }

    public int getQuantityNotSold() {
        return quantityNotSold;
    }

    public SpannableStringBuilder getQuantityStroke() {
        Log.d(TAG, "getQuantityStroke all: " + allQuantity + ", notSold " + quantityNotSold);
        if((allQuantity == quantityNotSold) ||
                (sizesNotSold != null && sizesSold != null &&
                    ((sizesNotSold.size() != 0 && !"".equals(sizesNotSold.get(0))) ||
                        (sizesSold.size() != 0 && !"".equals(sizesSold.get(0))))))
            return new SpannableStringBuilder(String.valueOf(quantityNotSold));

        SpannableStringBuilder b = new SpannableStringBuilder(String.valueOf(allQuantity));
        b.setSpan(new StrikethroughSpan(), 0, b.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        b.append(" ");
        b.append(String.valueOf(quantityNotSold));
        Log.d(TAG, "here stroke quantity " + model);
        return b;
    }

    public int getBuyPrice() {
        return buyPrice;
    }

    public String getBuyPriceString() {
        return String.valueOf(buyPrice);
    }

    public int getSalePrice() {
        return salePrice;
    }

    public String getSalePriceString() {
        return String.valueOf(salePrice);
    }

    public List<String> getSizesNotSold() {
        return sizesNotSold;
    }

    public List<String> getSizesSold() {
        return sizesSold;
    }

    public SpannableStringBuilder getSizesStroke() {
        if (debug) {
            Log.d(TAG, "sizesNotSold " + sizesNotSold);
            Log.d(TAG, "sizesSold " + sizesSold);
        }
        if (sizesNotSold == null && sizesSold == null)
            return new SpannableStringBuilder("-");
        SpannableStringBuilder b = new SpannableStringBuilder();
        if (sizesNotSold != null) {
            for (int i = 0; i < sizesNotSold.size(); i++) {
                b.append(sizesNotSold.get(i).trim());
                if(i != sizesNotSold.size() - 1)
                    b.append(", ");
            }
        }
        if (sizesSold != null && sizesSold.size() != 0) {
            for (String size : sizesSold) {
                if(!size.trim().equals("")) {
                    if(b.length() > 1)
                        b.append(", ");
                    b.append(size.trim());
                    b.setSpan(new StrikethroughSpan(), b.length() - size.length(), b.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                }
            }
        }
        return b;
    }

    public String getSizes() {
        StringBuilder b = new StringBuilder();
        if (sizesNotSold != null) {
            for (String size : sizesNotSold) {
                b.append(size);
                b.append(" ");
            }
            return b.toString();
        } else return "-";
    }

    public String getMarks() {
        return marks;
    }

    public void setSalePrice(int salePrice) {
        this.salePrice = salePrice;
    }

    public void setSizesNotSold(List<String> sizesNotSold) {
        if(sizesNotSold != null && sizesNotSold.size() > 0 && ",".equals(sizesNotSold.get(0)))
            sizesNotSold.remove(0);
        this.sizesNotSold = sizesNotSold;
    }

    public void setSizesSold(List<String> sizesSold) {
        if(sizesSold != null && sizesSold.size() > 0 && ",".equals(sizesSold.get(0)))
            sizesSold.remove(0);
        this.sizesSold = sizesSold;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setQuantityNotSold(int quantityNotSold) {
        this.quantityNotSold = quantityNotSold;
    }

    public boolean isInRange(String entranceDateFilter) {    //  check
        if (entranceDateFilter == null || "alltime".equals(entranceDateFilter))
            return true;
        int year, month, day;
        Calendar c = Calendar.getInstance();
        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH) + 1;
        day = c.get(Calendar.DAY_OF_MONTH);
        switch (entranceDateFilter) {
            case "day":
                return entranceDate.isInRange
                        (day > 1 ? new CalendarDay(year, month, day - 1) : // day < 7
                                        month > 0 ? new CalendarDay(year, month - 1, day + Utils.numberOfDaysInMonth(month - 1) - 1)
                                                : new CalendarDay(year - 1, 11, 31),
                                new CalendarDay(year, month, day));
            case "week":
                return entranceDate.isInRange
                        (day > 7 ? new CalendarDay(year, month, day - 8) : // day < 7
                                        month > 0 ? new CalendarDay(year, month - 1, day + Utils.numberOfDaysInMonth(month - 1) - 8)
                                                : new CalendarDay(year - 1, 11, day + 23),
                                new CalendarDay(year, month, day));
            case "month":
                return entranceDate.isInRange
                        (month > 0 ? new CalendarDay(year, month - 1, day) : new CalendarDay(year - 1, 11, day),
                                new CalendarDay(year, month, day));
            case "halfyear":
                return entranceDate.isInRange
                        (month > 5 ? new CalendarDay(year, month - 6, day) : new CalendarDay(year - 1, month + 6, day),
                                new CalendarDay(year, month, day));
            case "year":
                return entranceDate.isInRange(new CalendarDay(year - 1, month, day),
                        new CalendarDay(year, month, day));
            default:
                return true;
        }
    }

    protected Clothes(Parcel in) {
        id = in.readInt();
        name = in.readString();
        model = in.readString();
        if (in.readByte() == 0x01) {
            sizesSold = new ArrayList<>();
            in.readList(sizesSold, String.class.getClassLoader());
        } else {
            sizesSold = null;
        }
        if (in.readByte() == 0x01) {
            sizesNotSold = new ArrayList<>();
            in.readList(sizesNotSold, String.class.getClassLoader());
        } else {
            sizesNotSold = null;
        }
        allQuantity = in.readInt();
        quantityNotSold = in.readInt();
        buyPrice = in.readInt();
        salePrice = in.readInt();
        entranceDate = (CalendarDay) in.readValue(CalendarDay.class.getClassLoader());
        marks = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(model);
        if (sizesSold == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(sizesSold);
        }
        if (sizesNotSold == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(sizesNotSold);
        }
        dest.writeInt(allQuantity);
        dest.writeInt(quantityNotSold);
        dest.writeInt(buyPrice);
        dest.writeInt(salePrice);
        dest.writeValue(entranceDate);
        dest.writeString(marks);
    }

    public static final Parcelable.Creator<Clothes> CREATOR = new Parcelable.Creator<Clothes>() {
        @Override
        public Clothes createFromParcel(Parcel in) {
            return new Clothes(in);
        }

        @Override
        public Clothes[] newArray(int size) {
            return new Clothes[size];
        }
    };

    @Override
    public int compareTo(Clothes clothes) {
        if ((clothes.getEntranceDate().isAfter(entranceDate)))
            return 1;
        else
            return -1;
    }

    @Override
    public String toString() {
        return "Clothes{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", model='" + model + '\'' +
                ", sizesNotSold=" + sizesNotSold +
                ", sizesSold=" + sizesSold +
                ", allQuantity=" + allQuantity +
                ", quantityNotSold=" + quantityNotSold +
                ", buyPrice=" + buyPrice +
                ", salePrice=" + salePrice +
                ", entranceDate=" + entranceDate +
                ", marks='" + marks + '\'' +
                '}';
    }
}