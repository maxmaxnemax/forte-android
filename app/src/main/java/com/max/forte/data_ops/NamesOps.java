package com.max.forte.data_ops;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.max.forte.R;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class NamesOps {

    private static final String TAG = "NamesOps";
    private static final String NAMES_KEY = "names";
    private static final boolean debug = false;

    public static ArrayList<String> addName(Context ctx, ArrayList<String> namesList, String word) {
        if (debug)
            Log.d(TAG, "addName");
        Toast.makeText(ctx, ctx.getString(R.string.name_added).concat(": ") + word, Toast.LENGTH_SHORT).show();

        namesList.add(word);
        saveNames(ctx, namesList);
        return namesList;
    }

    public void removeName(Context context, ArrayList<String> namesList, String name) {
        for (String g : namesList) {
            if (name.equals(g)) {
                namesList.remove(g);
                saveNames(context, namesList);
            }
        }
    }

    public static void saveNames(Context ctx, List<String> namesList) {
        if (debug) {
            Log.d(TAG, "-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-==-=-=");
            Log.d(TAG, "saveNames");
        }
        SharedPreferences namesPrefs = ctx.getSharedPreferences(NAMES_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor e = namesPrefs.edit();
        Set<String> set = new HashSet<>();
        if (debug) {
            for (String name : namesList)
                Log.d(TAG, "name: " + name);
        }
        set.addAll(namesList);
        e.putStringSet(NAMES_KEY, set);
        e.apply();
    }

    public static boolean check(Context ctx, ArrayList<String> namesList, String word) {
        if (word.length() == 0) {
            Toast.makeText(ctx, ctx.getString(R.string.error_length), Toast.LENGTH_SHORT).show();
            return false;
        }
        for (String g : namesList) {
            if (word.equals(g)) {
                Toast.makeText(ctx, ctx.getString(R.string.name).concat(" ").concat(word).concat(" ").
                        concat(ctx.getString(R.string.repeated_name)), Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        return true;
    }

    public static ArrayList<String> getNames(Context ctx) {
        if (debug) {
            Log.d(TAG, "-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-==-=-=");
            Log.d(TAG, "getNames");
        }

        SharedPreferences namesPrefs = ctx.getSharedPreferences(NAMES_KEY, Context.MODE_PRIVATE);
        Set<String> set = namesPrefs.getStringSet(NAMES_KEY, null);
        if (set == null || set.size() == 0) {
            if (debug)
                Log.d(TAG, "null");
            return null;
        } else {
            if (debug)
                Log.d(TAG, "not null");
            ArrayList<String> namesList = new ArrayList<>(set);
            if (debug) {
                for (String name : namesList)
                    Log.d(TAG, "name: " + name);
            }
            return new ArrayList<>(set);
        }
    }
}
