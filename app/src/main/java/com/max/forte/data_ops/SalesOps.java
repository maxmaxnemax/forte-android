package com.max.forte.data_ops;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.max.forte.model.Sale;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class SalesOps {

    private static final String TAG = "SalesOps";
    private static final String SALES_KEY = "Sales";

    public static ArrayList<Sale> getSales(Context ctx) {
        Log.d(TAG, "-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-==-=-=");
        Log.d(TAG, "getSales");
        SharedPreferences salesPrefs = ctx.getSharedPreferences(SALES_KEY, Context.MODE_PRIVATE);
        ArrayList<Sale> salesList;
        if (salesPrefs.contains(SALES_KEY)) {
            String jsonSales = salesPrefs.getString(SALES_KEY, null);
            Gson gson = new Gson();
            Sale[] salesItems = gson.fromJson(jsonSales, Sale[].class);

            /*for(Sale sale : salesItems)
                Log.d(TAG, "sale: " + sale);*/

            salesList = new ArrayList<>(Arrays.asList(salesItems));
        } else
            salesList = new ArrayList<>();

        return salesList;
    }

    public static void saveSales(Context ctx, List<Sale> salesList) {
        Log.d(TAG, "-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-==-=-=");
        Log.d(TAG, "saveSales");
        SharedPreferences salesPrefs = ctx.getSharedPreferences(SALES_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = salesPrefs.edit();

        Gson gson = new Gson();
        String jsonSales = gson.toJson(salesList);

        ed.putString(SALES_KEY, jsonSales);
        ed.apply();

        /*for(Sale sale : salesList)
            Log.d(TAG, "sale " + sale.toString());*/
    }

}
