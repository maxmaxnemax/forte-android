package com.max.forte.data_ops;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.google.gson.Gson;
import com.max.forte.model.Clothes;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ClothesOps {

    private static final String TAG = "ClothesOps";
    private static final String CLOTHES_KEY = "Clothes";

    public static ArrayList<Clothes> getClothes(Context ctx) {
        Log.d(TAG, "-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-==-=-=");
        Log.d(TAG, "getClothes");
        SharedPreferences clothesPrefs = ctx.getSharedPreferences(CLOTHES_KEY, Context.MODE_PRIVATE);
        ArrayList<Clothes> clothesList;
        if (clothesPrefs.contains(CLOTHES_KEY)) {
            String jsonClothes = clothesPrefs.getString(CLOTHES_KEY, null);
            Gson gson = new Gson();
            Clothes[] clothesItems = gson.fromJson(jsonClothes, Clothes[].class);

            /*for(Clothes clothes : clothesItems)
                Log.d(TAG, "clothes: " + clothes);*/

            clothesList = new ArrayList<>(Arrays.asList(clothesItems));
        } else
            clothesList = new ArrayList<>();

        return clothesList;
    }

    public static void saveClothes(Context ctx, List<Clothes> clothesList) {
        Log.d(TAG, "-=-=-=-=-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-==-=-=");
        Log.d(TAG, "saveClothes");
        SharedPreferences clothesPrefs = ctx.getSharedPreferences(CLOTHES_KEY, Context.MODE_PRIVATE);
        SharedPreferences.Editor ed = clothesPrefs.edit();

        Gson gson = new Gson();
        String jsonClothes = gson.toJson(clothesList);

        ed.putString(CLOTHES_KEY, jsonClothes);
        ed.apply();

        /*for(Clothes clothes : clothesList)
            Log.d(TAG, "clothes " + clothes.toString());*/
    }

}
