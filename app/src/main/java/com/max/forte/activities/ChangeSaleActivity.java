package com.max.forte.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextUtils;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.max.forte.R;
import com.max.forte.Utils;
import com.max.forte.asyncTasks.UpdateSale;
import com.max.forte.model.Clothes;
import com.max.forte.model.Sale;
import com.max.forte.data_ops.ClothesOps;
import com.max.forte.view.StaticGridView;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ChangeSaleActivity extends Activity {

    private final Context ctx = this;
    private ArrayList<Clothes> clothesList;
    private ArrayList<Sale> salesList;
    private List<String> sizesNotSold, sizesSold;
    private String sizesText;

    private com.max.forte.model.CalendarDay entranceDate, saleDate;
    private Clothes selectedClothes;
    private Sale sale;
    private int quantityToSale, buyPrice;

    private Button setSizesButton, inputSaleDateButton;

    private EditText marks, salePriceEditText;
    private boolean areSizes;
    private String salePriceString;
    private static final String TAG = "ChangeSale";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        final Dialog dialog = new Dialog(ctx);
        dialog.setContentView(R.layout.change_sale_dialog);
        dialog.setTitle(getString(R.string.change_sale));
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
                finish();
            }
        });
        dialog.setCanceledOnTouchOutside(false);

        clothesList = getIntent().getParcelableArrayListExtra("clothesList");
        salesList = getIntent().getParcelableArrayListExtra("salesList");
        sale = getIntent().getParcelableExtra("sale");
        Log.d(TAG, "sale " + sale);
        for (Clothes c : clothesList) {
            Log.d(TAG, "c id " + c.getId());
            if (sale.getClothesId() == c.getId()) {
                selectedClothes = c;
                Log.d(TAG, "found clothes");
            }
        }
        quantityToSale = sale.getQuantity();
        sizesText = sale.getSizes();
        sizesNotSold = selectedClothes.getSizesNotSold();
        sizesSold = selectedClothes.getSizesSold();

        areSizes = (sizesSold != null && sizesSold.size() != 0 && !"".equals(sizesSold.get(0)));

        TextView nameTextView = (TextView) dialog.findViewById(R.id.name);
        nameTextView.setText(sale.getName());

        TextView modelTextView = (TextView) dialog.findViewById(R.id.model);
        modelTextView.setText(getString(R.string.model) + ": " + sale.getModel());

        TextView buyPriceTextView = (TextView) dialog.findViewById(R.id.price_buy);   //  set prices
        buyPrice = sale.getBuyPrice();
        buyPriceTextView.setText(getString(R.string.price_buy_hint) + " " + buyPrice);

        salePriceEditText = (EditText) dialog.findViewById(R.id.sale_price);
        salePriceString = sale.getSalePriceString();
        salePriceEditText.setText(salePriceString);

        setSizesButton = (Button) dialog.findViewById(R.id.input_sizes); //  set sizes
        if (areSizes)
            setSizesButtonText();
        else
            setQuantityButtonText();

        setSizesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (areSizes)
                    setSizes();
                else
                    setQuantity();
            }
        });

        marks = (EditText) dialog.findViewById(R.id.marks);  //  set marks
        marks.setText(sale.getMarks());

        TextView entranceDateTextView = (TextView) dialog.findViewById(R.id.entrance_date); //  set entrance date
        entranceDate = sale.getEntranceDate();
        entranceDateTextView.setText(getString(R.string.entrance_date) + "  ");
        Spannable spanEntranceDate = new SpannableString(sale.getEntranceDateString());
        spanEntranceDate.setSpan(new RelativeSizeSpan(1.3f), 0, sale.getEntranceDateString().length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        entranceDateTextView.append(spanEntranceDate);

        saleDate = sale.getSaleDate();
        inputSaleDateButton = (Button) dialog.findViewById(R.id.input_sale_date); //  set sale date
        inputSaleDateButton.setText(getString(R.string.sale_date) + " " + sale.getSaleDateString());
        inputSaleDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSaleDate();
            }
        });
        final Button saveClothes = (Button) dialog.findViewById(R.id.save_sale);   //   save clothes
        saveClothes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                quantityToSale = (sizesNotSold == null) ? quantityToSale : sizesNotSold.size();
                salePriceString = salePriceEditText.getText().toString().replaceAll("[^0-9]", "");
                if (salePriceString.length() > 1) {
                    if (selectedClothes.getSalePrice() == 0)
                        selectedClothes.setSalePrice(Integer.parseInt(salePriceString));
                    if (areSizes) {
                        if (sizesText.length() > 1) {

                            String[] newSizes = sizesText.split(" ");
                            List<String> oldSizes = sale.getSizesList();
                            for (String size : oldSizes) {
                                try {
                                    sizesSold.remove(size);
                                    sizesNotSold.add(size);
                                    selectedClothes.setQuantityNotSold(selectedClothes.getQuantityNotSold() + 1);
                                } catch (NumberFormatException nfe) {
                                }
                            }
                            for (String size : newSizes) {
                                try {
                                    sizesSold.add(size);
                                    sizesNotSold.remove(size);
                                    selectedClothes.setQuantityNotSold(selectedClothes.getQuantityNotSold() - 1);
                                } catch (NumberFormatException nfe) {
                                }
                            }
                            selectedClothes.setSizesNotSold(sizesNotSold);
                            if(sizesSold != null && sizesSold.size() > 0 && sizesSold.get(0).startsWith(","))
                                sizesSold.remove(0);
                            selectedClothes.setSizesSold(sizesSold);
                            saveSale(dialog);

                        } else
                            Toast.makeText(ctx, "�� �� ������� �������!", Toast.LENGTH_SHORT).show();
                    } else {
                        if (quantityToSale == 0) {
                            Toast.makeText(ctx, "�� �� ������� ���������� �����!", Toast.LENGTH_SHORT).show();
                        } else {
                            int diff = quantityToSale - sale.getQuantity();
                            selectedClothes.setQuantityNotSold(selectedClothes.getQuantityNotSold() - diff);
                            saveSale(dialog);
                        }
                    }
                } else
                    Toast.makeText(ctx, "�� �� ������� ���� �������!", Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    void saveSale(final Dialog dialog) {
        final Sale changedSale = new Sale(
                sale.getId(), sale.getClothesId(), sale.getName(), sale.getModel().replaceAll("[\\\\p{Alphabetic} ]*", ""),
                sizesText, areSizes ? sizesText.replaceAll("[^MLX0-9]", "").length() / 2 : quantityToSale,
                buyPrice, salePriceString.equals("") ? 0 : Integer.parseInt(salePriceString),
                entranceDate, saleDate == null ? new com.max.forte.model.CalendarDay(0, 0, 0) : saleDate,
                marks.getText().toString().replaceAll("[\\\\p{Alphabetic} ]*", "")
        );
        Log.d(TAG, "saleDate == null " + (saleDate == null));
        Log.d(TAG, "changedSale " + changedSale);
        if(Utils.isNetworkConnected(ctx)) {
            new UpdateSale() {
                @Override
                public void onPostExecute(Boolean updated) {
                    if (updated) {
                        clothesList.set(clothesList.indexOf(selectedClothes), selectedClothes);
                        salesList.set(salesList.indexOf(sale), changedSale);     //   save sales
                        ClothesOps.saveClothes(ctx, clothesList);  //      here posting clothes on server

                        Toast.makeText(ctx, getString(R.string.saved), Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                        Intent intent = new Intent();
                        intent.putExtra("clothesList", clothesList);
                        intent.putExtra("salesList", salesList);
                        intent.putExtra("name", sale.getName());
                        setResult(RESULT_OK, intent);
                        finish();

                    } else
                        Toast.makeText(ctx, getString(R.string.error), Toast.LENGTH_SHORT).show();
                }
            }.execute(new Pair(changedSale, selectedClothes));
        }
        else Toast.makeText(ctx, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
    }

    void setSizesButtonText() {
        if (sizesText != null && sizesText.length() > 1) {
            setSizesButton.setText(getString(R.string.sizes) + " " + sizesText);
        } else
            setSizesButton.setText(getString(R.string.set_sizes));
    }

    void setQuantityButtonText() {
        quantityToSale = sale.getQuantity();
        setSizesButton.setText(getString(R.string.quantity) + " " + quantityToSale);
    }

    void setQuantity() {
        sizesNotSold = sizesSold = null;
        final Dialog dialog = new Dialog(ctx);
        dialog.setContentView(R.layout.quantity_dialog);
        dialog.setTitle(getResources().getString(R.string.input_quantity));

        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.quantityNumberPicker);
        np.setMaxValue(selectedClothes.getAllQuantity() + sale.getQuantity());
        np.setMinValue(1);
        np.setValue(quantityToSale == 0 ? 1 : quantityToSale);
        np.setFocusable(true);
        np.setFocusableInTouchMode(true);

        Button dialogButton = (Button) dialog.findViewById(R.id.save_quantity);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "ok---");
                dialog.dismiss();
                setSizesButton.setText(getString(R.string.quantity) + " " + String.valueOf(np.getValue()));
                setSizesButton.setBackgroundColor(getResources().getColor(R.color.DarkCyan));
                saveQuantity(np.getValue());
            }
        });
        dialog.show();
    }

    void saveQuantity(int quantity) {
        quantityToSale = quantity;
    }

    void setSizes() {
        final Dialog dialog = new Dialog(ctx);
        dialog.setContentView(R.layout.sizes_dialog);
        StaticGridView sizesGrid = (StaticGridView) dialog.findViewById(R.id.sizes_grid);
        dialog.setTitle(getResources().getString(R.string.change_sizes));

        ArrayAdapter<String> sizesAdapter = new ArrayAdapter<>(ctx, android.R.layout.simple_list_item_1, selectedClothes.getSizesNotSold());
        final EditText sizesEditText = (EditText) dialog.findViewById(R.id.sizes_text);
        if (sizesText != null)
            sizesEditText.setText(sizesText);

        sizesGrid.setAdapter(sizesAdapter);

        sizesGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                String size = ((TextView) v).getText().toString();
                sizesEditText.append(size.concat(" "));
            }
        });

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sizes = sizesEditText.getText().toString().trim();
                if (sizes.replaceAll("[^MLX0-9]", " ").length() > 0) {
                    dialog.dismiss();
                    setSizesButton.setText(sizes);
                    setSizesButton.setEllipsize(TextUtils.TruncateAt.END);
                    setSizesButton.setMaxLines(1);
                    setSizesButton.setBackgroundColor(getResources().getColor(R.color.DarkCyan));
                    sizesText = sizes.replaceAll("[^MLX0-9]", " ").replaceAll(" +", " ").trim();
                    if (sizesText.startsWith(","))
                        sizesText = sizesText.replaceFirst(",", "").trim();
                } else
                    Toast.makeText(ctx, getString(R.string.input_sizes), Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    void setSaleDate() {
        final Dialog dialog = new Dialog(ctx);
        dialog.setContentView(R.layout.entrance_date_dialog);
        dialog.setTitle(getResources().getString(R.string.sale_date));

        final MaterialCalendarView calendarView = (MaterialCalendarView) dialog.findViewById(R.id.calendarView);
        Calendar calendar = Calendar.getInstance();
        if (saleDate == null)
            calendarView.setSelectedDate(new CalendarDay(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)));
        else
            calendarView.setSelectedDate(new CalendarDay(saleDate.getYear(), saleDate.getMonth() - 1, saleDate.getDay()));

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (calendarView.getSelectedDate() != null) {
                    saleDate = new com.max.forte.model.CalendarDay(calendarView.getSelectedDate().getYear(), calendarView.getSelectedDate().getMonth() + 1,
                            calendarView.getSelectedDate().getDay());
                    if (saleDate.isAfter(entranceDate)) {
                        inputSaleDateButton.setText(getString(R.string.sale_date) + " " + saleDate.toString());
                        inputSaleDateButton.setBackgroundColor(getResources().getColor(R.color.DarkCyan));
                    } else
                        Toast.makeText(ctx, "���� ������� ������ ���� �� ������ ���� �����������!", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}