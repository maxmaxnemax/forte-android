package com.max.forte.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.max.forte.R;
import com.max.forte.Utils;
import com.max.forte.adapters.SaleSizesAdapter;
import com.max.forte.asyncTasks.PostSale;
import com.max.forte.model.Clothes;
import com.max.forte.model.Sale;
import com.max.forte.data_ops.ClothesOps;
import com.max.forte.data_ops.NamesOps;
import com.max.forte.data_ops.SalesOps;
import com.max.forte.view.StaticGridView;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AddSaleActivity extends Activity { //  add sale

    private final Context ctx = this;
    private ArrayList<Clothes> clothesList, modelsForChosenName = new ArrayList<>();
    private ArrayList<Sale> salesList;
    private List<String> namesList, modelNamesForChosenName = new ArrayList<>(), sizesNotSold, sizesSold;

    private Spinner modelsSpinner;
    private int indexOfChosenName, clothesId;
    private String chosenName, sizesText = "", buyPriceString, salePriceString;
    private Button setSizesButton, inputSaleDateButton;
    private com.max.forte.model.CalendarDay entranceDate, saleDate;
    private TextView buyPriceTextView, entranceDateTextView;
    private EditText salePrice, marks;
    private Clothes selectedClothes;
    private int quantityNotSold, quantityToSale;
    private boolean areSizes;
    private static final String TAG = "AddSaleActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        final Dialog dialog = new Dialog(ctx);
        dialog.setContentView(R.layout.add_sale_dialog);
        dialog.setTitle(getString(R.string.add_sale));
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
                finish();
            }
        });
        dialog.setCanceledOnTouchOutside(false);

        clothesList = getIntent().getParcelableArrayListExtra("clothesList");
        salesList = getIntent().getParcelableArrayListExtra("salesList");
        if (salesList == null)
            salesList = new ArrayList<>();
        chosenName = getIntent().getStringExtra("name");

        Spinner nameSpinner = (Spinner) dialog.findViewById(R.id.names_spinner);
        namesList = NamesOps.getNames(ctx);

        if(chosenName == null)
            chosenName = namesList.get(0);

        ArrayAdapter<String> namesSpinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, namesList);
        namesSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        nameSpinner.setAdapter(namesSpinnerAdapter);
        if (chosenName != null) {
            indexOfChosenName = namesList.indexOf(chosenName);
            nameSpinner.setSelection(indexOfChosenName, true);
        }

        nameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                setModelAdapter(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });

        modelsSpinner = (Spinner) dialog.findViewById(R.id.models_spinner);    //  set models
        modelsSpinner.setSelection(0, true);
        modelsSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                setModelData(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {}
        });
        buyPriceTextView = (TextView) dialog.findViewById(R.id.price_buy);   //  prices
        salePrice = (EditText) dialog.findViewById(R.id.sale_price);

        setSizesButton = (Button) dialog.findViewById(R.id.input_sizes); //  set sizes
        setSizesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (areSizes)
                    setSizes();
                else if (quantityNotSold != 0)
                    setQuantity();
            }
        });

        entranceDateTextView = (TextView) dialog.findViewById(R.id.entrance_date);   //  entranceDate
        inputSaleDateButton = (Button) dialog.findViewById(R.id.input_sale_date); //  set sale date
        inputSaleDateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setSaleDate();
            }
        });
        marks = (EditText) dialog.findViewById(R.id.marks);  //  set marks

        Button saveSale = (Button) dialog.findViewById(R.id.save_sale);   //  save clothes
        saveSale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                buyPriceString = buyPriceTextView.getText().toString().replaceAll("[^0-9]", "");
                salePriceString = salePrice.getText().toString().replaceAll("[^0-9]", "");
                if(modelsSpinner.getSelectedItem() != null) {
                    if (salePriceString.length() > 1) {
                        if (selectedClothes.getSalePrice() == 0)
                            selectedClothes.setSalePrice(Integer.parseInt(salePriceString));
                        if (areSizes) {
                            if (sizesText.length() > 1) {

                                Log.d(TAG, "sizesText " + sizesText);
                                Log.d(TAG, "sizesSold " + sizesSold);
                                if (sizesSold == null)
                                    sizesSold = new ArrayList<>();
                                if(sizesSold.size() > 0 && "".equals(sizesSold.get(0)))
                                    sizesSold.remove(0);
                                String[] items = sizesText.split(" ");
                                for (String size : items) {
                                    Log.d(TAG, "size " + size);
                                    try {
                                        sizesSold.add(size);
                                        Log.d(TAG, "add size!!!" );
                                        sizesNotSold.remove(size);
                                        selectedClothes.setQuantityNotSold(selectedClothes.getQuantityNotSold() - 1);
                                        Log.d(TAG, "selectedClothes.getQuantityNotSold() - 1 " + (selectedClothes.getQuantityNotSold() - 1));
                                    } catch (NumberFormatException nfe) {
                                        nfe.printStackTrace();
                                    }
                                }
                                Log.d(TAG, "sizesSold " + sizesSold);
                                selectedClothes.setSizesNotSold(sizesNotSold);
                                selectedClothes.setSizesSold(sizesSold);
                                saveSale(dialog);

                            } else
                                Toast.makeText(ctx, "�� �� ������� �������!", Toast.LENGTH_SHORT).show();
                        } else {
                            if (quantityToSale == 0) {
                                Toast.makeText(ctx, "�� �� ������� ���������� �����!", Toast.LENGTH_SHORT).show();
                            } else {
                                selectedClothes.setQuantityNotSold(selectedClothes.getQuantityNotSold() - quantityToSale);
                                saveSale(dialog);
                            }
                        }
                    } else
                        Toast.makeText(ctx, "�� �� ������� ���� �������!", Toast.LENGTH_SHORT).show();
                } else
                    Toast.makeText(ctx, "�������� ������!", Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    void saveSale(final Dialog dialog) {
        if(chosenName == null)
            Toast.makeText(ctx, "�������� ������!", Toast.LENGTH_LONG).show();
        else {
            final Sale sale = new Sale(
                    -1, clothesId, chosenName, ((String) modelsSpinner.getSelectedItem()).replaceAll("[\\\\p{Alphabetic} ]*", ""),
                    sizesText, areSizes ? sizesText.replaceAll("[^MLX0-9]", "").length() / 2 : quantityToSale,
                    buyPriceString.equals("") ? 0 : Integer.parseInt(buyPriceString),
                    salePriceString.equals("") ? 0 : Integer.parseInt(salePriceString),
                    entranceDate, saleDate == null ? new com.max.forte.model.CalendarDay(0, 0, 0) : saleDate,
                    marks.getText().toString().replaceAll("[\\\\p{Alphabetic} ]*", "")
            );
            if(Utils.isNetworkConnected(ctx)) {
                new PostSale() {
                    @Override
                    public void onPostExecute(Integer id) {
                        if (id != -1) {
                            sale.setId(id);
                            salesList.add(sale);     //   save sales
                            clothesList.set(clothesList.indexOf(selectedClothes), selectedClothes);
                            ClothesOps.saveClothes(ctx, clothesList);  //      here posting clothes on server
                            SalesOps.saveSales(ctx, salesList);  //      here posting sales on server

                            Toast.makeText(ctx, getString(R.string.saved), Toast.LENGTH_LONG).show();
                            dialog.dismiss();
                            Intent intent = new Intent();
                            intent.putExtra("clothesList", clothesList);
                            intent.putExtra("salesList", salesList);
                            intent.putExtra("name", chosenName);
                            setResult(RESULT_OK, intent);
                            finish();
                        } else
                            Toast.makeText(ctx, getString(R.string.error), Toast.LENGTH_SHORT).show();
                    }
                }.execute(new Pair(sale, selectedClothes));
            }
            else Toast.makeText(ctx, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
        }
    }

    void setQuantity() {
        final Dialog dialog = new Dialog(ctx);
        sizesNotSold = sizesSold = null;
        sizesText = null;
        dialog.setContentView(R.layout.quantity_dialog);
        dialog.setTitle(getResources().getString(R.string.input_quantity));

        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.quantityNumberPicker);
        np.setMaxValue(quantityNotSold);
        np.setMinValue(1);
        np.setValue(1);
        np.setFocusable(true);
        np.setFocusableInTouchMode(true);

        Button dialogButton = (Button) dialog.findViewById(R.id.save_quantity);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "ok---");
                dialog.dismiss();
                setSizesButton.setText(getString(R.string.quantity) + " " + String.valueOf(np.getValue()));
                setSizesButton.setBackgroundColor(getResources().getColor(R.color.DarkCyan));
                saveQuantity(np.getValue());
            }
        });
        dialog.show();
    }

    void saveQuantity(int quantity) {
        quantityToSale = quantity;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {    //  set models adapter
        super.onPostCreate(savedInstanceState);
        Log.d(TAG, "onPostCreate");
        setModelAdapter(indexOfChosenName);
        //setModelData(0);
    }

    void setModelAdapter(int i) {
        modelsForChosenName.clear();
        modelNamesForChosenName.clear();
        Log.d(TAG, "setModelAdapter " + namesList.get(i));
        chosenName = namesList.get(i);
        for (Clothes c : clothesList) {
            Log.d(TAG, "c.areNotSoldClothes() " + c.areNotSoldClothes());
            Log.d(TAG, "c.getName() " + c.getName());
            if (c.areNotSoldClothes() && namesList.get(i).equals(c.getName())) {
                if (!modelsForChosenName.contains(c)) {
                    modelNamesForChosenName.add(c.getModel());
                    modelsForChosenName.add(c);
                }
            }
        }
        Log.d(TAG, "modelNamesForChosenName " + modelNamesForChosenName);
        ArrayAdapter<String> modelsSpinnerAdapter = new ArrayAdapter<>(ctx, android.R.layout.simple_spinner_item, modelNamesForChosenName);
        modelsSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        modelsSpinner.setAdapter(modelsSpinnerAdapter);
    }

    void setModelData(int i) {
        Log.d(TAG, "setModelData " + i);
        selectedClothes = modelsForChosenName.get(i);
        clothesId = selectedClothes.getId();
        entranceDate = selectedClothes.getEntranceDate();
        quantityNotSold = selectedClothes.getQuantityNotSold();
        sizesNotSold = selectedClothes.getSizesNotSold();
        sizesSold = selectedClothes.getSizesSold();
        Log.d(TAG, "selectedClothes " + selectedClothes);

        buyPriceTextView.setText(getString(R.string.price_buy) + " " + selectedClothes.getBuyPrice());
        salePriceString = selectedClothes.getSalePriceString();
        salePrice.setText(selectedClothes.getSalePriceString());

        areSizes = (sizesNotSold != null && sizesNotSold.size() != 0 && (!"".equals(sizesNotSold.get(0))));
        setSizesButton.setClickable(true);
        if (areSizes) {
            setSizesButton.setText(getString(R.string.set_sizes));
            for (String size : sizesNotSold)
                Log.d(TAG, "modelsForChosenName.get(i).getSizes() " + size);
        } else if (quantityNotSold == 1) {
            quantityToSale = 1;
            setSizesButton.setText(getString(R.string.quantity) + " 1");
            setSizesButton.setClickable(false);
            setSizesButton.setBackgroundColor(getResources().getColor(R.color.DarkCyan));
        } else {
            setSizesButton.setText(getString(R.string.quantity) + " 1");
            quantityToSale = 1;
        }

        entranceDateTextView.setText(getString(R.string.entrance_date) + " " + selectedClothes.getEntranceDateString());

        //Spannable span = new SpannableString(getString(R.string.change));
        //span.setSpan(new AbsoluteSizeSpan(12), 0, getString(R.string.change).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        saleDate = new com.max.forte.model.CalendarDay();
        inputSaleDateButton.setText(getString(R.string.sale_date) + " " + saleDate);
        //inputSaleDateButton.append(span);
        Log.d(TAG, "error no");
    }

    void setSizes() {
        final Dialog dialog = new Dialog(ctx);
        dialog.setContentView(R.layout.sizes_dialog_sales);
        final StaticGridView sizesGrid = (StaticGridView) dialog.findViewById(R.id.sizes_grid);
        dialog.setTitle(getResources().getString(R.string.input_sizes));

        ArrayAdapter<String> sizesAdapter = new SaleSizesAdapter(ctx, android.R.layout.simple_list_item_1, sizesNotSold);
        final TextView sizesTextView = (TextView) dialog.findViewById(R.id.sizes_text);

        sizesGrid.setAdapter(sizesAdapter);
        sizesGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                String size = ((TextView) v).getText().toString();
                sizesTextView.append(size.concat(" "));
                v.setBackgroundColor(getResources().getColor(R.color.DarkCyan));
                ((SaleSizesAdapter) sizesGrid.getAdapter()).addClick(position);
                ((SaleSizesAdapter) sizesGrid.getAdapter()).notifyDataSetChanged();
            }
        });

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sizes = sizesTextView.getText().toString().trim();
                if (sizes.replaceAll("[^MLX0-9]", " ").length() > 0) {
                    dialog.dismiss();
                    setSizesButton.setText(sizes);
                    setSizesButton.setEllipsize(TextUtils.TruncateAt.END);
                    setSizesButton.setMaxLines(1);
                    setSizesButton.setBackgroundColor(getResources().getColor(R.color.DarkCyan));
                    sizesText = sizes.replaceAll("[^MLX0-9]", " ").replaceAll(" +", " ").trim();
                    if (sizesText.startsWith(","))
                        sizesText = sizesText.replaceFirst(",", "").trim();
                } else
                    Toast.makeText(ctx, getString(R.string.input_sizes), Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    void setSaleDate() {
        final Dialog dialog = new Dialog(ctx);
        dialog.setContentView(R.layout.entrance_date_dialog);
        dialog.setTitle(getResources().getString(R.string.sale_date));

        final MaterialCalendarView calendarView = (MaterialCalendarView) dialog.findViewById(R.id.calendarView);
        Calendar calendar = Calendar.getInstance();
        if (saleDate == null)
            calendarView.setSelectedDate(new CalendarDay(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)));
        else
            calendarView.setSelectedDate(new CalendarDay(saleDate.getYear(), saleDate.getMonth() - 1, saleDate.getDay()));

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (calendarView.getSelectedDate() != null) {
                    saleDate = new com.max.forte.model.CalendarDay(calendarView.getSelectedDate().getYear(), calendarView.getSelectedDate().getMonth() + 1,
                            calendarView.getSelectedDate().getDay());
                    if (saleDate.isAfter(entranceDate)) {
                        inputSaleDateButton.setText(getString(R.string.sale_date) + " " + saleDate.toString());
                        inputSaleDateButton.setBackgroundColor(getResources().getColor(R.color.DarkCyan));
                    } else
                        Toast.makeText(ctx, "���� ������� ������ ���� �� ������ ���� �����������!", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}