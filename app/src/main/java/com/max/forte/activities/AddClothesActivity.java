package com.max.forte.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.max.forte.R;
import com.max.forte.Utils;
import com.max.forte.asyncTasks.PostClothes;
import com.max.forte.model.Clothes;
import com.max.forte.data_ops.ClothesOps;
import com.max.forte.data_ops.NamesOps;
import com.max.forte.view.StaticGridView;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class AddClothesActivity extends Activity {

    private final Context ctx = this;
    private ArrayList<Clothes> clothesList;
    private ArrayList<String> namesList, modelsForChosenName = new ArrayList<>();

    private AutoCompleteTextView model;
    private String sizesText, buyPriceString, salePriceString;
    private List<String> sizesNotSold;
    private com.max.forte.model.CalendarDay entranceDate;
    private Button setSizesButton;
    private EditText marks;

    private int indexOfChosenName, allQuantity, quantityNotSold;
    private String chosenName;

    private static final String[] ALL_SIZES = new String[]{
            "34", "36", "38", "40", "42",
            "44", "46", "48", "50", "52",
            "54", "56", "58", "60", "62", "M", "L", "XL", "XXL", "XXXL"};

    private static final String TAG = "AddClothes";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");

        final Dialog dialog = new Dialog(ctx);
        dialog.setContentView(R.layout.add_clothes_dialog);
        dialog.setTitle(getString(R.string.add_clothes));
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
                finish();
            }
        });
        dialog.setCanceledOnTouchOutside(false);

        clothesList = getIntent().getParcelableArrayListExtra("clothesList");
        chosenName = getIntent().getStringExtra("name");
        Log.d(TAG, "chosenName " + chosenName);

        Spinner nameSpinner = (Spinner) dialog.findViewById(R.id.names_spinner);
        namesList = NamesOps.getNames(ctx);

        Log.d(TAG, "namesList " + namesList);
        if(chosenName == null)
            chosenName = namesList.get(0);

        ArrayAdapter<String> namesSpinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, namesList);
        namesSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        nameSpinner.setAdapter(namesSpinnerAdapter);
        if (chosenName != null) {
            indexOfChosenName = namesList.indexOf(chosenName);
            nameSpinner.setSelection(indexOfChosenName, true);
        }
        nameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                Log.d(TAG, "onItemSelected " + namesList.get(i));
                setModelAdapter(i);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
            }
        });

        model = (AutoCompleteTextView) dialog.findViewById(R.id.model);  //  set models
        model.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                model.showDropDown();
                return false;
            }
        });
        setModelAdapter(indexOfChosenName);

        final EditText buyPrice = (EditText) dialog.findViewById(R.id.price_buy);   //  prices
        final EditText salePrice = (EditText) dialog.findViewById(R.id.sale_price);
        marks = (EditText) dialog.findViewById(R.id.marks);

        final CheckBox sizesCheckBox = (CheckBox) dialog.findViewById(R.id.sizesCheckBox);
        setSizesButton = (Button) dialog.findViewById(R.id.input_sizes); //  set sizes
        sizesCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean sizesEnabled) {
                if (sizesEnabled)
                    setSizesButtonText();
                else
                    setQuantityButtonText();
            }
        });

        setSizesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sizesCheckBox.isChecked())
                    setSizes();
                else
                    setQuantity();
            }
        });

        final Button inputEntranceDate = (Button) dialog.findViewById(R.id.input_entrance_date);    //  entrance date
        inputEntranceDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setEntranceDate(inputEntranceDate);
            }
        });

        final Button saveClothes = (Button) dialog.findViewById(R.id.save_clothes);   //  save clothes
        saveClothes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                buyPriceString = buyPrice.getText().toString().replaceAll("[^0-9]", "");
                salePriceString = salePrice.getText().toString().replaceAll("[^0-9]", "");
                allQuantity = (sizesNotSold == null) ? allQuantity : sizesNotSold.size();
                quantityNotSold = allQuantity;
                if (allQuantity == 0) {
                    if (sizesCheckBox.isChecked())
                        Toast.makeText(ctx, "������� �������!", Toast.LENGTH_LONG).show();
                    else
                        Toast.makeText(ctx, "������� ���������� �����!", Toast.LENGTH_LONG).show();
                } else
                    saveClothes(dialog, chosenName);
            }
        });
        dialog.show();
    }

    void saveClothes(final Dialog dialog, final String chosenName) {
        if (chosenName == null) {
            Log.d(TAG, "NUDDDLLL!");
            Toast.makeText(ctx, "�������� ������!", Toast.LENGTH_LONG).show();
        } else if(entranceDate != null){
            final Clothes clothes = new Clothes(
                    -1, chosenName, model.getText().toString().replaceAll("[\\\\p{Alphabetic} ]*", ""),
                    sizesNotSold, null, allQuantity, quantityNotSold,
                    buyPriceString.equals("") ? 0 : Integer.parseInt(buyPriceString),
                    salePriceString.equals("") ? 0 : Integer.parseInt(salePriceString),
                    entranceDate,
                    marks.getText() == null ? "" : marks.getText().toString().replaceAll("[\\\\p{Alphabetic} ]*", "")
            );
            Log.d(TAG, "clothes " + clothes);
            if (!clothesList.contains(clothes)) {
                if(Utils.isNetworkConnected(ctx)) {
                    new PostClothes() {
                        @Override
                        public void onPostExecute(Integer id) {
                            if (id != -1) {
                                clothes.setId(id);
                                clothesList.add(clothes);     //   save clothes
                                ClothesOps.saveClothes(ctx, clothesList);  //      here posting clothes on server
                                Toast.makeText(ctx, getString(R.string.saved), Toast.LENGTH_LONG).show();
                                dialog.dismiss();
                                Intent intent = new Intent();
                                intent.putExtra("clothesList", clothesList);
                                intent.putExtra("name", chosenName);
                                setResult(RESULT_OK, intent);
                                finish();
                            } else
                                Toast.makeText(ctx, ctx.getString(R.string.error), Toast.LENGTH_SHORT).show();
                        }
                    }.execute(clothes);
                }
                else Toast.makeText(ctx, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
            } else
                Toast.makeText(ctx, "������ ���� ������ � ����� ����� ����������� ��� ����!", Toast.LENGTH_LONG).show();
        }
        else
            Toast.makeText(ctx, "������� ���� �����������!", Toast.LENGTH_LONG).show();
    }

    void setSizesButtonText() {
        if (sizesText != null && sizesText.length() > 1)
            setSizesButton.setText(getString(R.string.change_sizes) + " " + sizesText);
        else
            setSizesButton.setText(getString(R.string.set_sizes));
    }

    void setQuantityButtonText() {
        if (allQuantity != 0)
            setSizesButton.setText(getString(R.string.change_quantity) + " " + allQuantity);
        else
            setSizesButton.setText(getString(R.string.input_quantity));
    }

    void setModelAdapter(int i) {
        modelsForChosenName.clear();
        chosenName = namesList.get(i);
        Log.d(TAG, "setModelAdapter " + namesList.get(i));
        for (Clothes c : clothesList)
            if (namesList.get(i).equals(c.getName())) {
                if (!modelsForChosenName.contains(c.getModel()))
                    modelsForChosenName.add(c.getModel());
            }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(ctx, android.R.layout.simple_dropdown_item_1line, modelsForChosenName);
        model.setAdapter(adapter);
    }

    void setSizes() {
        final Dialog dialog = new Dialog(ctx);
        dialog.setContentView(R.layout.sizes_dialog);
        StaticGridView sizesGrid = (StaticGridView) dialog.findViewById(R.id.sizes_grid);
        dialog.setTitle(getResources().getString(R.string.input_sizes));

        ArrayAdapter<String> sizesAdapter = new ArrayAdapter<>(ctx, android.R.layout.simple_list_item_1, ALL_SIZES);
        final EditText sizesEditText = (EditText) dialog.findViewById(R.id.sizes_text);
        if (sizesText != null)
            sizesEditText.setText(sizesText);

        sizesGrid.setAdapter(sizesAdapter);

        sizesGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                String size = ((TextView) v).getText().toString();
                sizesEditText.append(size.concat(" "));
            }
        });

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sizes = sizesEditText.getText().toString().trim();
                if (sizes.replaceAll("[^MLX0-9]", " ").length() > 0) {
                    dialog.dismiss();
                    setSizesButton.setText(sizes);
                    setSizesButton.setEllipsize(TextUtils.TruncateAt.END);
                    setSizesButton.setMaxLines(1);
                    setSizesButton.setBackgroundColor(getResources().getColor(R.color.DarkCyan));
                    sizesText = sizes.replaceAll("[^MLX0-9]", " ").replaceAll(" +", " ").trim();
                    String[] items = sizesText.split(" ");
                    sizesNotSold = new ArrayList<>();
                    for (String size : items) {
                        try {
                            sizesNotSold.add(size);
                        } catch (NumberFormatException e) {
                            e.printStackTrace();
                        }
                    }
                } else
                    Toast.makeText(ctx, getString(R.string.input_sizes), Toast.LENGTH_SHORT).show();
                Log.d(TAG, "size: " + sizesNotSold);
            }
        });
        dialog.show();
    }

    void setQuantity() {
        sizesNotSold = null;
        final Dialog dialog = new Dialog(ctx);
        dialog.setContentView(R.layout.quantity_dialog);
        dialog.setTitle(getResources().getString(R.string.input_quantity));

        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.quantityNumberPicker);
        np.setMaxValue(200);
        np.setMinValue(1);
        np.setValue(allQuantity == 0 ? 1 : allQuantity);
        np.setFocusable(true);
        np.setFocusableInTouchMode(true);

        Button dialogButton = (Button) dialog.findViewById(R.id.save_quantity);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "ok---");
                dialog.dismiss();
                setSizesButton.setText(getString(R.string.quantity) + " " + String.valueOf(np.getValue()));
                setSizesButton.setBackgroundColor(getResources().getColor(R.color.DarkCyan));
                saveQuantity(np.getValue());
            }
        });
        dialog.show();
    }

    void saveQuantity(int quantity) {
        this.allQuantity = quantity;
        quantityNotSold = quantity;
    }

    void setEntranceDate(final Button inputEntranceDate) {
        Log.d(TAG, "setEDate");
        final Dialog dialog = new Dialog(ctx);
        dialog.setContentView(R.layout.entrance_date_dialog);
        dialog.setTitle(getResources().getString(R.string.input_entrance_date));

        final MaterialCalendarView calendarView = (MaterialCalendarView) dialog.findViewById(R.id.calendarView);
        Calendar calendar = Calendar.getInstance();
        if (entranceDate == null)
            calendarView.setSelectedDate(new CalendarDay(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)));
        else
            calendarView.setSelectedDate(new CalendarDay(entranceDate.getYear(), entranceDate.getMonth() - 1, entranceDate.getDay()));

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (calendarView.getSelectedDate() != null && !calendarView.getSelectedDate().isAfter(new CalendarDay())) {
                    entranceDate = new com.max.forte.model.CalendarDay(calendarView.getSelectedDate().getYear(), calendarView.getSelectedDate().getMonth() + 1,
                            calendarView.getSelectedDate().getDay());
                    dialog.dismiss();
                    inputEntranceDate.setText(getString(R.string.entrance_date) + " " + entranceDate.toString());
                    inputEntranceDate.setBackgroundColor(getResources().getColor(R.color.DarkCyan));
                } else
                    Toast.makeText(ctx, "���� ����������� �� ����� ���� � �������!", Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }
}
