package com.max.forte.activities;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.max.forte.R;
import com.max.forte.Utils;
import com.max.forte.asyncTasks.UpdateClothes;
import com.max.forte.model.Clothes;
import com.max.forte.data_ops.ClothesOps;
import com.max.forte.data_ops.NamesOps;
import com.max.forte.view.StaticGridView;
import com.prolificinteractive.materialcalendarview.CalendarDay;
import com.prolificinteractive.materialcalendarview.MaterialCalendarView;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ChangeClothesActivity extends Activity {

    private static final String TAG = "ChangeClothes";
    private final Context ctx = this;
    private ArrayList<Clothes> clothesList;
    private List<String> namesList, modelsForChosenName = new ArrayList<>(), sizesNotSold, sizesSold;

    private  String sizesText, buyPriceString, salePriceString;
    private com.max.forte.model.CalendarDay entranceDate;

    private int allQuantity, quantityNotSold, position, id;

    private Spinner nameSpinner;
    private AutoCompleteTextView model;
    private Button setSizesButton;
    private EditText marks;
    private String chosenName;

    private static final String[] ALL_SIZES = new String[]{
            "34", "36", "38", "40", "42",
            "44", "46", "48", "50", "52",
            "54", "56", "58", "60", "62", "M", "L", "XL", "XXL", "XXXL"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        final Dialog dialog = new Dialog(ctx);
        dialog.setContentView(R.layout.add_clothes_dialog);
        dialog.setTitle(getString(R.string.change_clothes));
        dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                dialog.dismiss();
                finish();
            }
        });
        dialog.setCanceledOnTouchOutside(false);

        clothesList = getIntent().getParcelableArrayListExtra("clothesList");
        Clothes selectedClothes = getIntent().getParcelableExtra("clothes");
        chosenName = selectedClothes.getName();
        position = getIntent().getIntExtra("position", 0);

        id = selectedClothes.getId();
        allQuantity = selectedClothes.getAllQuantity();
        quantityNotSold = selectedClothes.getQuantityNotSold();
        sizesText = selectedClothes.getSizes();
        sizesNotSold = selectedClothes.getSizesNotSold();
        sizesSold = selectedClothes.getSizesSold();

        nameSpinner = (Spinner) dialog.findViewById(R.id.names_spinner);        //  set names
        namesList = NamesOps.getNames(ctx);

        ArrayAdapter<String> namesSpinnerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, namesList);
        namesSpinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        nameSpinner.setAdapter(namesSpinnerAdapter);
        nameSpinner.setSelection(namesList.indexOf(selectedClothes.getName()), true);

        nameSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                modelsForChosenName.clear();
                Log.d(TAG, "onItemSelected " + namesList.get(i));
                for (Clothes c : clothesList)
                    if (namesList.get(i).equals(c.getName())) {
                        if (!modelsForChosenName.contains(c.getModel()))
                            modelsForChosenName.add(c.getModel());
                    }
                ArrayAdapter<String> adapter = new ArrayAdapter<>(ctx, android.R.layout.simple_dropdown_item_1line, modelsForChosenName);
                model.setAdapter(adapter);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                Log.d(TAG, "onNothingSelected " + namesList.get(0));
            }
        });

        model = (AutoCompleteTextView) dialog.findViewById(R.id.model);
        model.setText(selectedClothes.getModel());
        model.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                model.showDropDown();
                return false;
            }
        });
        setModelAdapter(namesList.indexOf(chosenName));

        final EditText buyPrice = (EditText) dialog.findViewById(R.id.price_buy);   //  set prices
        buyPriceString = selectedClothes.getBuyPriceString();
        buyPrice.setHint(getString(R.string.price_buy_hint) + " " + buyPriceString);
        final EditText salePriceEditText = (EditText) dialog.findViewById(R.id.sale_price);
        salePriceString = selectedClothes.getSalePriceString();
        salePriceEditText.setHint(getString(R.string.sale_price_hint) + " " + salePriceString);

        setSizesButton = (Button) dialog.findViewById(R.id.input_sizes); //  set sizes
        final CheckBox sizesCheckBox = (CheckBox) dialog.findViewById(R.id.sizesCheckBox);  //  set sizesCheckbox
        sizesCheckBox.setChecked(sizesNotSold != null);
        if (sizesNotSold != null) {
            sizesCheckBox.setChecked(true);
            setSizesButtonText();
        } else {
            sizesCheckBox.setChecked(false);
            setQuantityButtonText();
        }
        sizesCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean sizesEnabled) {
                if (sizesEnabled)
                    setSizesButtonText();
                else
                    setQuantityButtonText();
            }
        });

        setSizesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sizesCheckBox.isChecked())
                    setSizes();
                else
                    setQuantity();
            }
        });

        marks = (EditText) dialog.findViewById(R.id.marks);  //  set marks
        marks.setText(selectedClothes.getMarks());

        final Button inputEntranceDate = (Button) dialog.findViewById(R.id.input_entrance_date);    //  set entrance date
        entranceDate = selectedClothes.getEntranceDate();
        inputEntranceDate.setText(getString(R.string.change_entrance_date) + ": " + entranceDate);
        inputEntranceDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setEntranceDate(inputEntranceDate);
            }
        });
        //String entranceDateString = selectedClothes.getEntranceDateString();
        //inputEntranceDate.append("\n");
        //inputEntranceDate.append(entranceDateString);//TODO
        //Spannable span = new SpannableString(entranceDateString);
        //span.setSpan(new RelativeSizeSpan(0.7f), 0, entranceDateString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        //inputEntranceDate.append(span);

        final Button saveClothes = (Button) dialog.findViewById(R.id.save_clothes);   //   save clothes
        saveClothes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String chosenName = nameSpinner.getSelectedItem().toString();
                buyPriceString = buyPrice.getText().toString().replaceAll("[^0-9]", "");
                salePriceString = salePriceEditText.getText().toString().replaceAll("[^0-9]", "");
                if (sizesCheckBox.isChecked()) {
                    sizesNotSold = new ArrayList<>();
                    String[] items = sizesText.split(" ");
                    for (String size : items) {
                        try {
                            sizesNotSold.add(size);
                        } catch (NumberFormatException nfe) {
                            nfe.printStackTrace();
                        }
                    }
                    quantityNotSold = sizesNotSold.size();
                    allQuantity = quantityNotSold + sizesSold.size();
                } else
                    sizesNotSold = sizesSold = null;
                saveClothes(dialog, chosenName);
            }
        });
        dialog.show();
    }

    void setModelAdapter(int i) {
        modelsForChosenName.clear();
        chosenName = namesList.get(i);
        Log.d(TAG, "setModelAdapter " + namesList.get(i));
        for (Clothes c : clothesList)
            if (namesList.get(i).equals(c.getName())) {
                if (!modelsForChosenName.contains(c.getModel()))
                    modelsForChosenName.add(c.getModel());
            }
        ArrayAdapter<String> adapter = new ArrayAdapter<>(ctx, android.R.layout.simple_dropdown_item_1line, modelsForChosenName);
        model.setAdapter(adapter);
    }

    void saveClothes(final Dialog dialog, final String chosenName) {
        final Clothes changedClothes = new Clothes(
                id, chosenName, model.getText().toString(),
                sizesNotSold, sizesSold, allQuantity, quantityNotSold,
                buyPriceString.equals("") ? 0 : Integer.parseInt(buyPriceString),
                salePriceString.equals("") ? 0 : Integer.parseInt(salePriceString),
                entranceDate == null ? new com.max.forte.model.CalendarDay(0, 0, 0) : entranceDate,
                marks.getText() == null ? "" : marks.getText().toString()
        );
        if(Utils.isNetworkConnected(ctx)) {
            new UpdateClothes() {
                @Override
                public void onPostExecute(Boolean updated) {
                    if (updated) {
                        clothesList.set(position, changedClothes);     //   save changedClothes
                        ClothesOps.saveClothes(ctx, clothesList);  //      here posting changedClothes on server

                        Toast.makeText(ctx, getString(R.string.saved), Toast.LENGTH_LONG).show();
                        dialog.dismiss();
                        Intent intent = new Intent();
                        intent.putExtra("clothesList", clothesList);
                        if (chosenName != null)
                            intent.putExtra("name", chosenName);
                        setResult(RESULT_OK, intent);
                        finish();
                    } else
                        Toast.makeText(ctx, getString(R.string.error), Toast.LENGTH_SHORT).show();
                }
            }.execute(changedClothes);
        }
        else Toast.makeText(ctx, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
    }

    void setSizesButtonText() {
        if (sizesText != null && sizesText.length() > 1) {
            //Spannable span = new SpannableString(getString(R.string.change));
            //span.setSpan(new RelativeSizeSpan(0.7f), 0, getString(R.string.change).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            setSizesButton.setText(getString(R.string.sizes) + " " + sizesText);
        } else
            setSizesButton.setText(getString(R.string.set_sizes));
    }

    void setQuantityButtonText() {
        if (allQuantity != 0) {
            //Spannable span = new SpannableString(getString(R.string.change));
            //span.setSpan(new RelativeSizeSpan(0.7f), 0, getString(R.string.change).length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            setSizesButton.setText(getString(R.string.quantity) + " " + allQuantity);
        } else
            setSizesButton.setText(getString(R.string.input_quantity));
    }

    void setQuantity() {
        final Dialog dialog = new Dialog(ctx);
        dialog.setContentView(R.layout.quantity_dialog);
        dialog.setTitle(getResources().getString(R.string.input_quantity));

        final NumberPicker np = (NumberPicker) dialog.findViewById(R.id.quantityNumberPicker);
        np.setMaxValue(200);
        np.setMinValue(1);
        np.setValue(allQuantity == 0 ? 1 : allQuantity);
        np.setFocusable(true);
        np.setFocusableInTouchMode(true);

        Button dialogButton = (Button) dialog.findViewById(R.id.save_quantity);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "ok---");
                dialog.dismiss();
                setSizesButton.setText(getString(R.string.quantity) + " " + String.valueOf(np.getValue()));
                setSizesButton.setBackgroundColor(getResources().getColor(R.color.DarkCyan));
                sizesNotSold = sizesSold = null;
                saveQuantity(np.getValue());
            }
        });
        dialog.show();
    }

    void saveQuantity(int quantity) {
        this.allQuantity = quantity;
    }

    void setSizes() {
        final Dialog dialog = new Dialog(ctx);
        dialog.setContentView(R.layout.sizes_dialog);
        StaticGridView sizesGrid = (StaticGridView) dialog.findViewById(R.id.sizes_grid);
        dialog.setTitle(getResources().getString(R.string.change_sizes));

        ArrayAdapter<String> sizesAdapter = new ArrayAdapter<>(ctx, android.R.layout.simple_list_item_1, ALL_SIZES);
        final EditText sizesEditText = (EditText) dialog.findViewById(R.id.sizes_text);
        if (sizesText != null)
            sizesEditText.setText(sizesText);

        sizesGrid.setAdapter(sizesAdapter);

        sizesGrid.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                String size = ((TextView) v).getText().toString();
                sizesEditText.append(size.concat(" "));
            }
        });

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String sizes = sizesEditText.getText().toString().trim();
                if (sizes.replaceAll("[^MLX0-9]", " ").length() > 0) {
                    dialog.dismiss();
                    setSizesButton.setText(sizes);
                    setSizesButton.setEllipsize(TextUtils.TruncateAt.END);
                    setSizesButton.setMaxLines(1);
                    setSizesButton.setBackgroundColor(getResources().getColor(R.color.DarkCyan));
                    sizesText = sizes.replaceAll("[^MLX0-9]", " ").replaceAll(" +", " ").trim();
                } else
                    Toast.makeText(ctx, getString(R.string.input_sizes), Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }

    void setEntranceDate(final Button setEntranceDate) {
        final Dialog dialog = new Dialog(ctx);
        dialog.setContentView(R.layout.entrance_date_dialog);
        dialog.setTitle(getResources().getString(R.string.input_entrance_date));

        final MaterialCalendarView calendarView = (MaterialCalendarView) dialog.findViewById(R.id.calendarView);
        Calendar calendar = Calendar.getInstance();
        if (entranceDate == null)
            calendarView.setSelectedDate(new CalendarDay(calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH)));
        else
            calendarView.setSelectedDate(new CalendarDay(entranceDate.getYear(), entranceDate.getMonth() - 1, entranceDate.getDay()));

        Button dialogButton = (Button) dialog.findViewById(R.id.dialogButtonOK);
        dialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (calendarView.getSelectedDate() != null && !calendarView.getSelectedDate().isAfter(new CalendarDay())) {
                    entranceDate = new com.max.forte.model.CalendarDay(calendarView.getSelectedDate().getYear(), calendarView.getSelectedDate().getMonth() + 1,
                            calendarView.getSelectedDate().getDay());
                    dialog.dismiss();
                    setEntranceDate.setText(getString(R.string.entrance_date) + " " + entranceDate.toString());
                    setEntranceDate.setBackgroundColor(getResources().getColor(R.color.DarkCyan));
                } else
                    Toast.makeText(ctx, "���� ����������� �� ����� ���� � �������!", Toast.LENGTH_SHORT).show();
            }
        });
        dialog.show();
    }
}
