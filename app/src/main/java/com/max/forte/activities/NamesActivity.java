package com.max.forte.activities;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.max.forte.MainActivity;
import com.max.forte.R;
import com.max.forte.Utils;
import com.max.forte.adapters.NamesAdapter;
import com.max.forte.asyncTasks.PostName;
import com.max.forte.data_ops.NamesOps;
import com.max.forte.view.RecView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class NamesActivity extends AppCompatActivity {

    private static final String TAG = "GA";
    NamesAdapter namesAdapter;

    private ArrayList<String> namesList = new ArrayList<>();

    Context ctx = this;
    WeakReference<Context> cReference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.names);

        Toolbar toolbar = (Toolbar) findViewById(R.id.app_bar);
        setSupportActionBar(toolbar);

        Log.d(TAG, "onCreate");
        cReference = new WeakReference<>(ctx);

        namesList = NamesOps.getNames(ctx);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.names));

        RecView recyclerView = (RecView) findViewById(R.id.list);
        namesAdapter = new NamesAdapter(getNamesList(), this);

        RecyclerView.ItemAnimator itemAnimator = new DefaultItemAnimator();
        recyclerView.setEmptyView(findViewById(R.id.empty));
        recyclerView.setAdapter(namesAdapter);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.setItemAnimator(itemAnimator);
        registerForContextMenu(recyclerView);

        if (namesList.size() == 0)
            namesList = NamesOps.getNames(ctx);
    }

    List<String> getNamesList() {
        return namesList;
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.clear();
        getMenuInflater().inflate(R.menu.names_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(TAG, "uuu");
        switch (item.getItemId()) {
            case R.id.add_name:

                final EditText input_name = new EditText(this);
                input_name.setInputType(InputType.TYPE_CLASS_TEXT);
                new AlertDialog.Builder(this)
                        .setTitle(getString(R.string.add_to_names))
                        .setMessage(getString(R.string.input_name))
                        .setView(input_name)
                        .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                if (Utils.isNetworkConnected(ctx)) {
                                    final String name = input_name.getText().toString().replaceAll("[\\\\p{Alphabetic} ]*", "");
                                    if (namesList == null)
                                        namesList = new ArrayList<>();
                                    if (NamesOps.check(ctx, namesList, name)) {
                                        new PostName() {
                                            @Override
                                            public void onPostExecute(Boolean added) {
                                                if (added) {
                                                    namesList = NamesOps.addName(cReference.get(), namesList, input_name.getText().toString());
                                                    input_name.setText("");
                                                    namesAdapter.notifyDataSetChanged();
                                                } else
                                                    Toast.makeText(ctx, getString(R.string.error), Toast.LENGTH_SHORT).show();
                                            }
                                        }.execute(name);
                                    }
                                }
                                else Toast.makeText(ctx, getString(R.string.no_internet), Toast.LENGTH_SHORT).show();
                            }
                        }).setNegativeButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int whichButton) {
                        dialog.dismiss();
                    }
                }).show();
                return true;

            case R.id.refresh:
                namesList = NamesOps.getNames(ctx);
                namesAdapter.notifyChanges(getNamesList());
                return true;

            case android.R.id.home:
                Log.d(TAG, "jkjolj");
                goBack();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void goBack() {
        Intent intent = new Intent(ctx, MainActivity.class);
        finish();
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        goBack();
    }
}
